package br.com.helpsocorrista.helpsocorrista.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = StatusSolicitacao.TABLE_NAME)
public class StatusSolicitacao implements Parcelable, Serializable, TabelaAuxiliar {
    public static final String TABLE_NAME       = "status_solicitacao";
    public static final String COLUMN_ID        = "id";
    public static final String COLUMN_DESCRICAO = "descricao";

    @SerializedName("id")
    @Expose
    @DatabaseField(id = true, canBeNull = false, columnName = COLUMN_ID)
    private Integer id;

    @SerializedName("descricao")
    @Expose
    @DatabaseField(canBeNull = false,columnName = COLUMN_DESCRICAO)
    private String descricao;

    //costrutor vazio

    public StatusSolicitacao(){}

    // Getter and Setter

    @Override
    public String getTextoCurto() {
        return null;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeString(this.descricao);
    }

    protected StatusSolicitacao(Parcel in){
        this.id = (Integer) in.readSerializable();
        this.descricao = in.readString();
    }

    public static final Parcelable.Creator<StatusSolicitacao> CREATOR = new Parcelable.Creator<StatusSolicitacao>(){

        @Override
        public StatusSolicitacao createFromParcel(Parcel source) {
            return new StatusSolicitacao(source);
        }

        @Override
        public StatusSolicitacao[] newArray(int size) {
            return new StatusSolicitacao[size];
        }
    };
}
