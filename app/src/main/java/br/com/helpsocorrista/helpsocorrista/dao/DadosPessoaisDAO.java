package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;


import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;


public class DadosPessoaisDAO {

    private Dao<DadosPessoais, Integer> dao;

    private Dao<DadosPessoais, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(DadosPessoais.class);
        }
        return dao;
    }

    public void save(DadosPessoais dadosPessoais) throws SQLException{
        if(dadosPessoais.getId() == null || dadosPessoais.getId() == 0){
            getDao().create(dadosPessoais);
        } else {
            if (getById(dadosPessoais.getId()) == null) {
                getDao().create(dadosPessoais);
            } else {
                getDao().update(dadosPessoais);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }

    public DadosPessoais getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

    public DadosPessoais getByUserId(Integer id) throws SQLException {
        return getDao().queryBuilder().where().eq(DadosPessoais.COLUMN_USERS_ID, id).queryForFirst();
    }

    public DadosPessoais getTelefone(String telefone) throws SQLException {
        return getDao().queryBuilder()
                .where()
                .eq(DadosPessoais.COLUMN_TELEFONE, telefone).queryForFirst();
    }

}
