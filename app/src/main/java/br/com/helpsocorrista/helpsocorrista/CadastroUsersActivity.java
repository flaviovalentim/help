package br.com.helpsocorrista.helpsocorrista;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.sql.SQLException;
import java.util.Random;

import br.com.helpsocorrista.helpsocorrista.dao.DBHelper;
import br.com.helpsocorrista.helpsocorrista.Helpers.ValidaCPF;
import br.com.helpsocorrista.helpsocorrista.dao.UsersDAO;
import br.com.helpsocorrista.helpsocorrista.model.Users;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import okhttp3.OkHttpClient;


public class CadastroUsersActivity extends AppCompatActivity {

    private EditText txtCpfUsers;
    private EditText txtSenhaUsers;
    private EditText txtConfSenhaUsers;
    private CheckBox txtTermosUso;
    private TextView txtAcessTermo;
    private Button   btnSalvarUsers;
    private Button   btnCancelarUsers;
    private Crouton crouton;

    final int min = 100000;
    final int max = 999999;

    UsersDAO usersDAO = new UsersDAO();
    Users usuario     = new Users();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro__users);

        txtCpfUsers          = (EditText)findViewById(R.id.txtCpfUsers);
        txtSenhaUsers        = (EditText)findViewById(R.id.txtSenhaUsers);
        txtConfSenhaUsers    = (EditText)findViewById(R.id.txtConfSenhaUsers);
        txtTermosUso         = (CheckBox)findViewById(R.id.txtTermosUso);
        txtAcessTermo        = (TextView)findViewById(R.id.txtAcessTermo);
        btnSalvarUsers       = (Button)findViewById(R.id.btnSalvarUsers);
        btnCancelarUsers     = (Button)findViewById(R.id.btnCancelarUsers);

        //INICIO DA MASCARA
        SimpleMaskFormatter smf = new SimpleMaskFormatter("NNN.NNN.NNN-NN");
        MaskTextWatcher mtw     = new MaskTextWatcher(txtCpfUsers, smf);
        txtCpfUsers.addTextChangedListener(mtw);
        //FIM DA MASCARA

        final int random = new Random().nextInt((max - min) + 1) + min;



        btnSalvarUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
//                byte[] encodSenha = Base64.encode(txtSenhaUsers.getText().toString().getBytes(), Base64.DEFAULT);
//                byte[] decodSenha = Base64.decode(encodSenha.getBytes(), Base64.DEFAULT);


                String cpf              = String.valueOf(txtCpfUsers.getText().toString().replaceAll("[^0-9]", ""));
                String senhaUsers       = txtSenhaUsers.getText().toString().replaceAll("[^0-9]", "");
                String confSenhaUsers   = txtConfSenhaUsers.getText().toString().replaceAll("[^0-9]", "");
                String termosUso        = txtTermosUso.isChecked() ? "1" : "0";
                String novaSenha        = String.valueOf(random);

                if (cpf.equals("")) {
                    msgCpf();
                    return;
                }
                if (senhaUsers.equals("")) {
                    msgSenha();
                    return;
                }
                if (senhaUsers.length() <= 5){
                    msgTamanhoSenha();
                    return;
                }
                if (confSenhaUsers.equals("")){
                    msgSenhaConf();
                    return;
                }
                if (confSenhaUsers.length()<=5){
                    msgSenhaTamanhoConfirm();
                    return;
                }
                if (!senhaUsers.equals(confSenhaUsers)) {
                    msgSenhaConfDif();
                    return;
                }
                if(!ValidaCPF.isCPF(cpf)){
                    msgCpfIvalido();
                    return;
                }

                UsersDAO usersDAO = new UsersDAO();
                Users usuarioUnico = null;
                try {
                    usuarioUnico = usersDAO.getCpf(cpf);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (usuarioUnico != null) {
                    msgExisteCpf();
                    return;
                }
                if (termosUso == "0"){
                    msgTermosUso();
                    return;
                }
                else {
                    usuario.setCpf(cpf);
                    usuario.setSenhaMobile(senhaUsers);
                    usuario.setSenha_reset(novaSenha);
                    try {
                        usersDAO.save(usuario);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    msgUserCadSucesso();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                        }
                    }, 2000L);
                }
            }
        });

        txtAcessTermo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CadastroUsersActivity.this, TermoUsoActivity.class);
                startActivity(i);
            }
        });

        btnCancelarUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CadastroUsersActivity.this, PrincipalActivity.class);
                startActivity(i);
                finish();
            }
        });

    }
    public void onDestroy(){
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    /*mensagem de alerta*/
    public void msgCpf(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Obrigatório informar CPF.", Style.ALERT);
        crouton.show();
    }
    public void msgSenha(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Obrigatório informar Senha.", Style.ALERT);
        crouton.show();
    }

    public void msgTamanhoSenha(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Senha deve conter 6 dígitos.", Style.ALERT);
        crouton.show();
    }
    public void msgSenhaConf(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Obrigatório informar Senha de confirmação.", Style.ALERT);
        crouton.show();
    }
    public void msgSenhaTamanhoConfirm(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Senha de confirmação deve conter 6 dígitos.", Style.ALERT);
        crouton.show();
    }
    public void msgSenhaConfDif(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Senha não correspondem.", Style.ALERT);
        crouton.show();
    }
    public void msgCpfIvalido(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "CPF inválido. Favor informar novamente.", Style.ALERT);
        crouton.show();
    }
    public void msgExisteCpf(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "CPF informado já cadastrado.", Style.ALERT);
        crouton.show();
    }

    public void msgTermosUso(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "É necessário aceitar os termos de uso.", Style.ALERT);
        crouton.show();
    }
    public void msgUserCadSucesso(){
        crouton = Crouton.makeText(CadastroUsersActivity.this, "Usuário Cadastrado com sucesso.", Style.CONFIRM);
        crouton.show();
    }
}

