package br.com.helpsocorrista.helpsocorrista.Services.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import br.com.helpsocorrista.helpsocorrista.model.Doencas;

public class DoencaDto {

    @SerializedName("current_page")
    private Integer current_page;

    @SerializedName("data")
    ArrayList<Doencas> data;

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public ArrayList<Doencas> getData() {
        return data;
    }

    public void setData(ArrayList<Doencas> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DoencaDto{" +
                "current_page=" + current_page +
                ", data=" + data +
                '}';
    }
}
