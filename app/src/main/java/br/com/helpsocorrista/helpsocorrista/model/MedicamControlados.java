package br.com.helpsocorrista.helpsocorrista.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = MedicamControlados.TABLE_NAME)
public class MedicamControlados implements Parcelable, Serializable, TabelaAuxiliar {
    public static final String TABLE_NAME       = "medicamentos";
    public static final String COLUMN_ID        = "id";
    public static final String COLUMN_DESCRICAO = "descricao";

    @SerializedName("id")
    @Expose
    @DatabaseField(id = true, canBeNull = false, columnName = COLUMN_ID)
    private Integer id;

    @SerializedName("descricao")
    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_DESCRICAO)
    private String descricao;

    //construtor vazio
    public MedicamControlados(){}

    public MedicamControlados(Integer id, String descricao){
        this.id         = id;
        this.descricao  = descricao;
    }

    @Override
    public String getTextoCurto() {
        return this.descricao;
    }

    // Getter and Setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeString(this.descricao);

    }

    protected MedicamControlados(Parcel in) {
        this.id         = (Integer) in.readSerializable();
        this.descricao  = in.readString();
    }

    public static final Parcelable.Creator<MedicamControlados> CREATOR = new Parcelable.Creator<MedicamControlados>(){

        @Override
        public MedicamControlados createFromParcel(Parcel source) {
            return new MedicamControlados(source);
        }

        @Override
        public MedicamControlados[] newArray(int size) {
            return new MedicamControlados[size];
        }
    };
}
