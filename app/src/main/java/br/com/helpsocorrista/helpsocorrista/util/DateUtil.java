package br.com.helpsocorrista.helpsocorrista.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static Date convertStringInDate(String dateString){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = null;
        try {
            startDate = df.parse(dateString);

            return startDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (startDate == null) ? new Date() : startDate;
    }
}
