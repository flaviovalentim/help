package br.com.helpsocorrista.helpsocorrista.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = MedicamControladosUsers.TABLE_NAME)
public class MedicamControladosUsers implements Parcelable, Serializable {

    public static final String TABLE_NAME                   = "medicamentos_users";
    public static final String COLUMN_ID                    = "id";
    public static final String COLUMN_MEDICAM_CONTROL_ID    = "mediam_controlados_id";
    public static final String COLUMN_USERS_ID              = "users_id";


    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_MEDICAM_CONTROL_ID)
    private MedicamControlados mediam_controlados_id;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_ID)
    private Users users_id;

    //construtor vazio
    public MedicamControladosUsers(){}

    // Getter and Setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MedicamControlados getMediam_controlados_id() {
        return mediam_controlados_id;
    }

    public void setMediam_controlados_id(MedicamControlados mediam_controlados_id) {
        this.mediam_controlados_id = mediam_controlados_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeParcelable(this.mediam_controlados_id, flags);
        dest.writeParcelable(this.users_id, flags);
    }

    protected MedicamControladosUsers(Parcel in) {
    this.id                         = (Integer) in.readSerializable();
        this.mediam_controlados_id  = in.readParcelable(MedicamControlados.class.getClassLoader());
        this.users_id               = in.readParcelable(Users.class.getClassLoader());
    }

    public static final Creator<MedicamControladosUsers> CREATOR = new Creator<MedicamControladosUsers>() {
        @Override
        public MedicamControladosUsers createFromParcel(Parcel source) {
            return new MedicamControladosUsers(source);
        }

        @Override
        public MedicamControladosUsers[] newArray(int size) {
            return new MedicamControladosUsers[size];
        }
    };
}
