package br.com.helpsocorrista.helpsocorrista.Services.Interfaces;

import org.json.JSONObject;

import br.com.helpsocorrista.helpsocorrista.Services.Auth.AccessToken;
import br.com.helpsocorrista.helpsocorrista.model.Users;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceLogin {

    @POST("login")
    Call<AccessToken> login(@Body Users usuario);
}
