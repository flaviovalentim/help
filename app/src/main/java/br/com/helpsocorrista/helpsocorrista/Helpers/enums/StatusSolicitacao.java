package br.com.helpsocorrista.helpsocorrista.Helpers.enums;

public enum StatusSolicitacao {
    P("Pendente"),
    EA("Em Atendimento"),
    AV("A Verificar");

    private String status;

    private StatusSolicitacao(String status){
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
