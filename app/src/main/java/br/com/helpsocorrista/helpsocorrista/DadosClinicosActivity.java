package br.com.helpsocorrista.helpsocorrista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.helpsocorrista.helpsocorrista.Helpers.enums.TipoSanguineo;
import br.com.helpsocorrista.helpsocorrista.Services.Sync;
import br.com.helpsocorrista.helpsocorrista.adapter.AuxiliarAdapter;
import br.com.helpsocorrista.helpsocorrista.dao.AlergiasDAO;
import br.com.helpsocorrista.helpsocorrista.dao.AlergiasUsersDAO;
import br.com.helpsocorrista.helpsocorrista.dao.DadosPessoaisDAO;
import br.com.helpsocorrista.helpsocorrista.model.Alergias;
import br.com.helpsocorrista.helpsocorrista.model.AlergiasUsers;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;
import br.com.helpsocorrista.helpsocorrista.model.Doencas;
import br.com.helpsocorrista.helpsocorrista.model.MedicamControlados;
import br.com.helpsocorrista.helpsocorrista.model.MedicamControladosUsers;
import br.com.helpsocorrista.helpsocorrista.util.CustomSection;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public
class DadosClinicosActivity extends AppCompatActivity {
    private AuxiliarAdapter         adapterDoencas;
    private AuxiliarAdapter         adapterAlergias;
    private AuxiliarAdapter         adapterMedicamCotrolados;

    private ListView                listAlegiasDc;
    private ListView                listMedicamentosDc;
    private ListView                listDoencasDc;
    private Button                  btnAlergiasDc;
    private Button                  btnMedicamentosDc;
    private Button                  btnDoecasDc;
    private Button                  btnCancelarDc;
    private Button                  btnSalvarDc;
    private Spinner                 myTypoSanguineo;
    private DadosPessoais           dadosPessoais;
    private TextView                txtLayoutDadosClinicos;
    private Crouton                 crouton;

    private ArrayList<String>       listAlergia;
    private ArrayList<String>       listDoenca;
    private ArrayList<String>       listMedicamento;
    private ArrayAdapter<String>    itensAdapterAlergia;
    private ArrayAdapter<String>    itensAdapterMedicamento;
    private ArrayAdapter<String>    itensAdapterDoenca;

    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent i = getIntent();
        dadosPessoais = (DadosPessoais) i.getSerializableExtra("dados");
        setContentView(R.layout.activity_dados__clinicos);

        btnAlergiasDc       = (Button)findViewById(R.id.btnAlergiasDc);
        btnMedicamentosDc   = (Button)findViewById(R.id.btnMedicamentosDc);
        btnDoecasDc         = (Button)findViewById(R.id.btnDoecasDc);
        btnSalvarDc         = (Button)findViewById(R.id.btnSalvarDc);
        btnCancelarDc       = (Button)findViewById(R.id.btnCancelarDc);
        listAlegiasDc       = (ListView)findViewById(R.id.listAlegiasDc);
        listMedicamentosDc  = (ListView)findViewById(R.id.listMedicamentosDc);
        listDoencasDc       = (ListView)findViewById(R.id.listDoencasDc);

        myTypoSanguineo     = (Spinner)findViewById(R.id.txtTpSangue);
        myTypoSanguineo.setAdapter(new ArrayAdapter<TipoSanguineo>(this, android.R.layout.simple_list_item_1, TipoSanguineo.values()));

        final Spinner myDoencas   = (Spinner) findViewById(R.id.txtDoencas);
        adapterDoencas      = new AuxiliarAdapter(DadosClinicosActivity.this, android.R.layout.simple_list_item_1, new Sync().syncListDoencas());
        myDoencas.setAdapter(adapterDoencas);

        final Spinner myAlergias  = (Spinner) findViewById(R.id.txtAlergias);
        adapterAlergias     = new AuxiliarAdapter(DadosClinicosActivity.this, android.R.layout.simple_list_item_1, new Sync().syncListAlergias());
        myAlergias.setAdapter(adapterAlergias);

        final Spinner myMedicamComtrolados    = (Spinner) findViewById(R.id.txtMedControlados);
        adapterMedicamCotrolados        = new AuxiliarAdapter(DadosClinicosActivity.this, android.R.layout.simple_list_item_1, new Sync().syncListMecicamControlados());
        myMedicamComtrolados.setAdapter(adapterMedicamCotrolados);


        listAlergia             = new ArrayList<String>();
        itensAdapterAlergia     = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listAlergia);
        listAlegiasDc.setAdapter(itensAdapterAlergia);

        btnAlergiasDc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myAlergias.getSelectedItemId() == -1){
                    crouton = Crouton.makeText(DadosClinicosActivity.this, "Caso não possua Alergia, Selecione Não possuo.", Style.ALERT);
                    crouton.show();
                    return;
                }
                if (myAlergias.getSelectedItemId() >= 1){
                    Alergias a = (Alergias) myAlergias.getSelectedItem();
                    if(!listAlergia.contains(a.getDescricao())) {
                        listAlergia.add(a.getDescricao());
                        itensAdapterAlergia.notifyDataSetChanged();
                    }else{
                        crouton = Crouton.makeText(DadosClinicosActivity.this, "Alergia " + a.getDescricao() + " já existe na lista.", Style.ALERT);
                        crouton.show();
                    }
                    return;
                }
            }
        });

        listAlegiasDc.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    listAlergia.remove(position);
                    itensAdapterAlergia.notifyDataSetChanged();

                return true;
            }
        });

        listMedicamento             = new ArrayList<String>();
        itensAdapterMedicamento     = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listMedicamento);
        listMedicamentosDc.setAdapter(itensAdapterMedicamento);

        btnMedicamentosDc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myMedicamComtrolados.getSelectedItemId() == -1){
                    crouton = Crouton.makeText(DadosClinicosActivity.this,"Caso não use medicamentos, Selecione Não faço uso.", Style.ALERT);
                    crouton.show();
                    return;
                }
                if (myMedicamComtrolados.getSelectedItemId() >= 1){
                    MedicamControlados m = (MedicamControlados) myMedicamComtrolados.getSelectedItem();
                    if (!listMedicamento.contains(m.getDescricao())){
                        listMedicamento.add(m.getDescricao());
                        itensAdapterMedicamento.notifyDataSetChanged();
                    }else {
                        crouton = Crouton.makeText(DadosClinicosActivity.this,"Medicamento " + m.getDescricao() + " já existe na lista.", Style.ALERT);
                        crouton.show();
                    }
                    return;
                }
            }
        });

        listMedicamentosDc.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listMedicamento.remove(position);
                itensAdapterMedicamento.notifyDataSetChanged();
                return true;
            }
        });

        listDoenca      = new ArrayList<String>();
        itensAdapterDoenca    = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listDoenca);
        listDoencasDc.setAdapter(itensAdapterDoenca);

        btnDoecasDc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myDoencas.getSelectedItemId() == -1){
                    crouton = Crouton.makeText(DadosClinicosActivity.this,"Caso não tenha Doença, Selecione Não possuo", Style.ALERT);
                    crouton.show();
                    return;
                }
                if (myDoencas.getSelectedItemId() >= 1){
                    Doencas d = (Doencas) myDoencas.getSelectedItem();
                    if (!listDoenca.contains(d.getDescricao())){
                        listDoenca.add(d.getDescricao());
                        Log.i("TESTE", String.valueOf(d.getId()));
                        itensAdapterDoenca.notifyDataSetChanged();
                        Log.i("Doenca", String.valueOf(myDoencas.getSelectedItemId()));
                    }else {
                        crouton = Crouton.makeText(DadosClinicosActivity.this,"Doença " + d.getDescricao() + " já existe na lista.", Style.ALERT);
                        crouton.show();
                    }
                    return;
                }
            }
        });

        listDoencasDc.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listDoenca.remove(position);
                itensAdapterDoenca.notifyDataSetChanged();
                return false;

            }
        });

        btnSalvarDc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (myTypoSanguineo.getSelectedItemId() == 0) {
                    crouton = Crouton.makeText(DadosClinicosActivity.this,"Caso não saiba, Selecione Não sei Informar.", Style.ALERT);
                    crouton.show();
                    return;
                }

                dadosPessoais.setTipoSanguineo((TipoSanguineo)myTypoSanguineo.getSelectedItem());

                try {
                    new DadosPessoaisDAO().save(dadosPessoais);
                    Toast.makeText(getApplicationContext(), "Dados com sucesso", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), SolicitacaoActivity.class);
                    startActivity(i);
                    finish();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        });
        btnCancelarDc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DadosClinicosActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    public void onDestroy(){
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }
}
