package br.com.helpsocorrista.helpsocorrista.Services;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.helpsocorrista.helpsocorrista.Services.Interfaces.ServiceAlergias;
import br.com.helpsocorrista.helpsocorrista.Services.Interfaces.ServiceDoencas;
import br.com.helpsocorrista.helpsocorrista.Services.Interfaces.ServiceMedicamentos;
import br.com.helpsocorrista.helpsocorrista.Services.dto.AlergiaDto;
import br.com.helpsocorrista.helpsocorrista.Services.dto.DoencaDto;
import br.com.helpsocorrista.helpsocorrista.Services.dto.MedicamentoDto;
import br.com.helpsocorrista.helpsocorrista.dao.AlergiasDAO;
import br.com.helpsocorrista.helpsocorrista.dao.DoencasDAO;
import br.com.helpsocorrista.helpsocorrista.dao.MedicamControladosDAO;
import br.com.helpsocorrista.helpsocorrista.model.Alergias;
import br.com.helpsocorrista.helpsocorrista.model.Doencas;
import br.com.helpsocorrista.helpsocorrista.model.MedicamControlados;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sync {

    static AlergiaDto       alergias;
    static DoencaDto        doencas;
    static MedicamentoDto   medicamentos;

    public Sync(){

    }

    public static void syncDoencas(){
        ServiceDoencas serviceDoencas = RetrofitClientInstance.getRetrofitInstance().create(ServiceDoencas.class);
        Call<DoencaDto> call = serviceDoencas.listar();
        call.enqueue(new Callback<DoencaDto>() {
            @Override
            public void onResponse(Call<DoencaDto> call, Response<DoencaDto> response) {

                doencas = response.body();
                ArrayList<Doencas> doenc = doencas.getData();

                DoencasDAO dao = new DoencasDAO();

                for (int i =0; i < doenc.size(); i++){
                    try{
                        dao.save(doenc.get(i));
                    }catch (java.sql.SQLException e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<DoencaDto> call, Throwable t) {

            }
        });
    }

    public List<Doencas> syncListDoencas(){
        List<Doencas> listDoencas =  new ArrayList<>();

        try {
            listDoencas = new DoencasDAO().getAllToSpinner();

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

        return listDoencas;
    }

    public static void syncAlergias(){
        ServiceAlergias serviceAlergias = RetrofitClientInstance.getRetrofitInstance().create(ServiceAlergias.class);
        Call<AlergiaDto> call = serviceAlergias.listar();
        call.enqueue(new Callback<AlergiaDto>() {
            @Override
            public void onResponse(Call<AlergiaDto> call, Response<AlergiaDto> response) {

                alergias = response.body();
                ArrayList<Alergias> alerg = alergias.getData();

                AlergiasDAO dao = new AlergiasDAO();

                for (int i =0; i < alerg.size(); i++){
                    try{
                        dao.save(alerg.get(i));
                    }catch (java.sql.SQLException e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<AlergiaDto> call, Throwable t) {
            }
        });
    }

    public List<Alergias> syncListAlergias(){
        List<Alergias> listAlergias =  new ArrayList<>();

        try {

            listAlergias = new AlergiasDAO().getAllToSpinner();

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        Log.e("Alergias", String.valueOf(listAlergias.size()));
        return listAlergias;
    }

    public static void syncMedicamControlados(){
        ServiceMedicamentos serviceMedicamentos = RetrofitClientInstance.getRetrofitInstance().create(ServiceMedicamentos.class);
        Call<MedicamentoDto> call = serviceMedicamentos.listar();
        call.enqueue(new Callback<MedicamentoDto>() {
            @Override
            public void onResponse(Call<MedicamentoDto> call, Response<MedicamentoDto> response) {

                medicamentos = response.body();
                ArrayList<MedicamControlados> medica = medicamentos.getDate();

                MedicamControladosDAO dao = new MedicamControladosDAO();

                for (int i =0; i < medica.size(); i++){
                    try{
                        dao.save(medica.get(i));
                    }catch (java.sql.SQLException e){
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<MedicamentoDto> call, Throwable t) {
            }
        });
    }

    public List<MedicamControlados> syncListMecicamControlados(){
        List<MedicamControlados> listMedicamControlados = new ArrayList<>();

        try {

            listMedicamControlados = new MedicamControladosDAO().getAllToSpinner();

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

        return listMedicamControlados;

    }
}

