package br.com.helpsocorrista.helpsocorrista;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;


import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.dao.DadosPessoaisDAO;
import br.com.helpsocorrista.helpsocorrista.dao.ImagemUserDAO;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;
import br.com.helpsocorrista.helpsocorrista.model.ImagemUser;
import br.com.helpsocorrista.helpsocorrista.model.Solicitacao;
import br.com.helpsocorrista.helpsocorrista.model.Users;
import br.com.helpsocorrista.helpsocorrista.util.CustomSection;
import de.hdodenhof.circleimageview.CircleImageView;

public class SolicitacaoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private CircleImageView     txtImagemUser;
    private TextView            txtUsersCadastrado;
    private TextView            txtCpfCadastrado;
    private String              mCurrentPhotoPath;
    private ImagemUser          imagemUser;
    private MapView             mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1IjoiZmxhdmlvdmFsZW50aW0iLCJhIjoiY2prOHF1NzZkMm43eTNxbnRuZTg1eGt5NSJ9.C11JA2kTQfZGkN-Omkje2w");
        setContentView(R.layout.activity_solicitacao);
        //Inicio mapbox
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);


        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mapboxMap.setStyle(Style.LIGHT, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                    // Map is set up and the style has loaded. Now you can add data or make other map adjustments

                    }
                });
            }
        });

        //Fim mapbox

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SolicitacaoActivity.this, RealizarSolicitacaoActivity.class);
                startActivity(i);
                finish();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        txtImagemUser       = (CircleImageView) header.findViewById(R.id.txtImagemUser);
        txtUsersCadastrado  = (TextView) header.findViewById(R.id.txtUsersCadastrado);
        txtCpfCadastrado    = (TextView) header.findViewById(R.id.txtCpfCadastrado);

        //INICIO DA MASCARA
        SimpleMaskFormatter smf = new SimpleMaskFormatter("NNN.NNN.NNN-NN");
        MaskTextWatcher mtw     = new MaskTextWatcher(txtCpfCadastrado, smf);
        txtCpfCadastrado.addTextChangedListener(mtw);
        //FIM DA MASCARA


        try {
            Users usuarioLogado = CustomSection.getInstance().getLoggedUser();
            if(usuarioLogado != null){
                DadosPessoais dadosPessoais = new DadosPessoaisDAO().getByUserId(usuarioLogado.getId());
                if(dadosPessoais != null){
                    txtUsersCadastrado.setText(dadosPessoais.getNome());
                    txtCpfCadastrado.setText(usuarioLogado.getCpf());

                }else{
                    txtUsersCadastrado.setText("JOSE FERREIRA");
                    txtCpfCadastrado.setText("000.000.000-00");
                }
            }
            else{
                txtUsersCadastrado.setText("JOSE FERREIRA");
                txtCpfCadastrado.setText("000.000.000-00");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Users usuarioLogado = CustomSection.getInstance().getLoggedUser();
        if (usuarioLogado != null) {
            try {
                imagemUser = new ImagemUserDAO().getByUserId(usuarioLogado.getId());

                if (imagemUser != null && imagemUser.getCaminho() != null) {
                    mCurrentPhotoPath = imagemUser.getCaminho();
                    configureView();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        txtImagemUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SolicitacaoActivity.this, ImagemUserActivity.class );
                startActivity(i);

            }
        });
    }

    private void configureView(){
        File file = new File(mCurrentPhotoPath);
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        if (bitmap != null) {
            txtImagemUser.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.solicitacao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent i = new Intent(SolicitacaoActivity.this, ImagemUserActivity.class );
            startActivity(i);

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_send) {
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Deseja realmente sair do Help")
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                this.finish();
                            }

                            private void finish() {
                                this.finish();
                            }
                        })
                        .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//  Inicio para o mapbox
    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    //  Fim para o mapbox

}

