package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.model.DoencasUsers;

public class DoencasUsersDAO {

    private Dao<DoencasUsers, Integer> dao;

    private Dao<DoencasUsers, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(DoencasUsers.class);
        }
        return dao;
    }

    public void save(DoencasUsers doencasUsers) throws SQLException{
        if(doencasUsers.getId() == null || doencasUsers.getId() == 0){
            getDao().create(doencasUsers);
        } else {
            if (getById(doencasUsers.getId()) == null) {
                getDao().create(doencasUsers);
            } else {
                getDao().update(doencasUsers);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public DoencasUsers getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }
}
