package br.com.helpsocorrista.helpsocorrista.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = EndSolicitacao.TABLE_NAME)
public class EndSolicitacao implements Parcelable, Serializable{
    public static final String TABLE_NAME               = "end_solicitacao";
    public static final String COLUMN_ID                = "id";
    public static final String COLUMN_LOGRADOURO        = "logradouro";
    public static final String COLUMN_BAIRRO            = "bairro";
    public static final String COLUMN_NUMERO            = "numero";
    public static final String COLUMN_CEP               = "cep";
    public static final String COLUMN_PONTOREFERENCIA   = "ponto_referencia";
    public static final String COLUMN_COMPLEMENTO       = "complemento";
    public static final String COLUMN_LATITUDE          = "latitude";
    public static final String COLUMN_LONGITUDE         = "longitude";

    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_LOGRADOURO)
    private String logradouro;

    @Expose
    @DatabaseField(columnName = COLUMN_BAIRRO)
    private String bairro;

    @Expose
    @DatabaseField(columnName = COLUMN_NUMERO)
    private int numero;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_CEP)
    private String cep;

    @Expose
    @DatabaseField(columnName = COLUMN_PONTOREFERENCIA)
    private String pontoReferencia;

    @Expose
    @DatabaseField(columnName = COLUMN_COMPLEMENTO)
    private String complemento;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_LATITUDE)
    private String latitude;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_LONGITUDE)
    private String longitude;

    //construtor vazio
    public EndSolicitacao(){}

    // Getter and Setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getPontoReferencia() {
        return pontoReferencia;
    }

    public void setPontoReferencia(String pontoReferencia) {
        this.pontoReferencia = pontoReferencia;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeString(this.logradouro);
        dest.writeString(this.bairro);
        dest.writeInt(this.numero);
        dest.writeString(this.cep);
        dest.writeString(this.pontoReferencia);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
    }

    protected EndSolicitacao(Parcel in) {
        this.id                 = (Integer) in.readSerializable();
        this.logradouro         = in.readString();
        this.bairro             = in.readString();
        this.numero             = in.readInt();
        this.cep                = in.readString();
        this.pontoReferencia    = in.readString();
        this.latitude           = in.readString();
        this.logradouro         = in.readString();
    }

    public static final Creator<EndSolicitacao> CREATOR = new Creator<EndSolicitacao>() {
        @Override
        public EndSolicitacao createFromParcel(Parcel source) {
            return new EndSolicitacao(source);
        }

        @Override
        public EndSolicitacao[] newArray(int size) {
            return new EndSolicitacao[size];
        }
    };
}
