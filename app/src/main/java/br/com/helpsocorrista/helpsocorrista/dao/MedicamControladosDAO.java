package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.helpsocorrista.helpsocorrista.model.MedicamControlados;


public class MedicamControladosDAO {

    private Dao<MedicamControlados, Integer> dao;

    private Dao<MedicamControlados, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(MedicamControlados.class);
        }
        return dao;
    }

    public void save(MedicamControlados medicamControlados) throws SQLException{
        if(medicamControlados.getId() == null || medicamControlados.getId() == 0){
            getDao().create(medicamControlados);
        } else {
            if (getById(medicamControlados.getId()) == null) {
                getDao().create(medicamControlados);
            } else {
                getDao().update(medicamControlados);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public MedicamControlados getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

    public List<MedicamControlados> getAllToSpinner() throws SQLException {
        List<MedicamControlados> medicamControlados = new ArrayList<>();
        medicamControlados.add(new MedicamControlados(-1, "Selecione"));
        medicamControlados.addAll(getDao().queryBuilder().orderBy(MedicamControlados.COLUMN_DESCRICAO, true).query());
        return medicamControlados;
    }
}
