package br.com.helpsocorrista.helpsocorrista.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = DoencasUsers.TABLE_NAME)
public class DoencasUsers implements Parcelable, Serializable{

    public static final String TABLE_NAME           = "doencas_users";
    public static final String COLUMN_ID            = "id";
    public static final String COLUMN_DOENCAS_ID    = "doencas_id";
    public static final String COLUMN_USERS_ID      = "users_id";

    @Expose
    @DatabaseField(generatedId = true, canBeNull = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_DOENCAS_ID)
    private Doencas doencas_id;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_ID)
    private Users users_id;


    //construtor vazio
    public DoencasUsers(){}

    // Getter and Setter


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Doencas getDoencas_id() {
        return doencas_id;
    }

    public void setDoencas_id(Doencas doencas_id) {
        this.doencas_id = doencas_id;
    }

    public Users getUsers_Id() {
        return users_id;
    }

    public void setUsers_Id(Users users_Id) {
        this.users_id = users_Id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeParcelable(this.doencas_id, flags);
        dest.writeParcelable(this.users_id, flags);
    }

    protected DoencasUsers(Parcel in) {
        this.id             = (Integer) in.readSerializable();
        this.doencas_id     = in.readParcelable(Doencas.class.getClassLoader());
        this.users_id       = in.readParcelable(Users.class.getClassLoader());
    }

    public static final Creator<DoencasUsers> CREATOR = new Creator<DoencasUsers>() {
        @Override
        public DoencasUsers createFromParcel(Parcel source) {
            return new DoencasUsers(source);
        }

        @Override
        public DoencasUsers[] newArray(int size) {
            return new DoencasUsers[size];
        }
    };
}
