package br.com.helpsocorrista.helpsocorrista.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = Alergias.TABLE_NAME)
public class Alergias implements Parcelable, Serializable, TabelaAuxiliar {
    public static final String TABLE_NAME       = "alergias";
    public static final String COLUMN_ID        = "id";
    public static final String COLUMN_DESCRICAO = "descricao";


    @SerializedName("id")
    @Expose
    @DatabaseField(id = true, canBeNull = false, columnName = COLUMN_ID)
    private Integer id;

    @SerializedName("descricao")
    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_DESCRICAO)
    private String descricao;
    //construtor vazio

    public Alergias(){}

    public Alergias(Integer id, String descricao){
        this.id         = id;
        this.descricao  = descricao;
    }

    @Override
    public String getTextoCurto() {
        return this.descricao;
    }

    // Getter and Setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeString(this.descricao);
    }

    protected Alergias(Parcel in) {
        this.id         = (Integer) in.readSerializable();
        this.descricao  = in.readString();
    }

    public static final Parcelable.Creator<Alergias> CREATOR = new Parcelable.Creator<Alergias>(){

        @Override
        public Alergias createFromParcel(Parcel source) {
            return new Alergias(source);
        }

        @Override
        public Alergias[] newArray(int size) {
            return new Alergias[size];
        }
    };
}
