package br.com.helpsocorrista.helpsocorrista.dao;


import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.helpsocorrista.helpsocorrista.model.Doencas;

public class DoencasDAO {

    private Dao<Doencas, Integer> dao;

    private Dao<Doencas, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(Doencas.class);
        }
        return dao;
    }

    public void save(Doencas doencas) throws SQLException{
        if(doencas.getId() == null || doencas.getId() == 0){
            getDao().create(doencas);
        } else {
            if (getById(doencas.getId()) == null) {
                getDao().create(doencas);
            } else {
                getDao().update(doencas);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public Doencas getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

    public List<Doencas> getAllToSpinner() throws SQLException {
        List<Doencas> doencas = new ArrayList<>();
        doencas.add(new Doencas(-1, "Selecione"));
        doencas.addAll(getDao().queryBuilder().orderBy(Doencas.COLUMN_DESCRICAO, true).query());

        return doencas;
    }
}
