package br.com.helpsocorrista.helpsocorrista.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = AlergiasUsers.TABLE_NAME)
public class AlergiasUsers implements Parcelable, Serializable {

    public static final String TABLE_NAME           = "alergias_users";
    public static final String COLUMN_ID            = "id";
    public static final String COLUMN_ALERGIAS_ID   = "alergias_id";
    public static final String COLUMN_USERS_ID      = "users_id";

    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_ALERGIAS_ID)
    private Alergias alergias_id;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_ID)
    private Users users_id;


    //construtor vazio
    public AlergiasUsers(){}


    // Getter and Setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Alergias getAlergias_id() {
        return alergias_id;
    }

    public void setAlergias_id(Alergias alergias_id) {
        this.alergias_id = alergias_id;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeParcelable(this.alergias_id, flags);
        dest.writeParcelable(this.users_id, flags);
    }

    protected AlergiasUsers(Parcel in) {
        this.id             = (Integer) in.readSerializable();
        this.alergias_id    = in.readParcelable(Alergias.class.getClassLoader());
        this.users_id       = in.readParcelable(Users.class.getClassLoader());
    }

    public static final Creator<AlergiasUsers> CREATOR = new Creator<AlergiasUsers>() {
        @Override
        public AlergiasUsers createFromParcel(Parcel source) {
            return new AlergiasUsers(source);
        }

        @Override
        public AlergiasUsers[] newArray(int size) {
            return new AlergiasUsers[size];
        }
    };
}
