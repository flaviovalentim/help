package br.com.helpsocorrista.helpsocorrista.Services.Interfaces;

import java.util.List;

import br.com.helpsocorrista.helpsocorrista.Services.dto.DoencaDto;
import br.com.helpsocorrista.helpsocorrista.model.Doencas;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ServiceDoencas {

//    @Headers({
//            "Authorization: Token e4bf771bbc5916073f8f3d1777a861e89eb71a67"
//    })

    @GET("doenca")
    Call<DoencaDto> listar();
}
