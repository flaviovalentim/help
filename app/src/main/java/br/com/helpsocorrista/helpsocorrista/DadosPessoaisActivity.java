package br.com.helpsocorrista.helpsocorrista;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.github.rtoshiro.util.format.text.SimpleMaskTextWatcher;

import java.util.Calendar;
import java.util.Date;

import br.com.helpsocorrista.helpsocorrista.Helpers.enums.Sexo;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;
import br.com.helpsocorrista.helpsocorrista.util.CustomSection;
import br.com.helpsocorrista.helpsocorrista.util.StringUtil;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class DadosPessoaisActivity extends AppCompatActivity {
    private static final String TAG = "DadosPessoaisActivity";

    private Button                              btnAvencarDp;
    private Button                              btnCancelarDp;
    private EditText                            txtTelefoneDp;
    private EditText                            txtTelFamiliaDp;
    private EditText                            txtNomeDp;
    private EditText                            txtNumCartaoSusDp;
    private EditText                            txtAlturaDp;
    private EditText                            txtPesoDp;
    private CheckBox                            txtDoadorOrgaosDp;
    private EditText                            mDataNascimento;
    private DatePickerDialog.OnDateSetListener  mDateSetListener;
    private Spinner                             mySexo;
    private Crouton                             crouton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_pessoais);
        btnCancelarDp       = (Button)findViewById(R.id.btnCancelarDc);
        btnAvencarDp        = (Button)findViewById(R.id.btnSalvarDc);
        txtTelefoneDp       = (EditText)findViewById(R.id.txtTelefoneDp);
        txtTelFamiliaDp     = (EditText)findViewById(R.id.txtTelFamiliaDp);
        txtNomeDp           = (EditText)findViewById(R.id.txtNomeDp);
        txtNumCartaoSusDp   = (EditText)findViewById(R.id.txtNunCartaoSusDp);
        txtAlturaDp         = (EditText)findViewById(R.id.txtAlturaDp);
        txtPesoDp           = (EditText)findViewById(R.id.txtPesoDp);
        txtDoadorOrgaosDp   = (CheckBox)findViewById(R.id.txtDoadorOrgaos);
        mDataNascimento     = (EditText)findViewById(R.id.DataNascimento);
        mySexo              = (Spinner) findViewById(R.id.txtSexo);
        mySexo.setAdapter(new ArrayAdapter<Sexo>(this, android.R.layout.simple_list_item_1, Sexo.values()));


        mDataNascimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public
            void onClick(View v) {
                calendar();
            }
        });

        //INICIO DA MASCARA
        SimpleMaskFormatter teldp   = new SimpleMaskFormatter("(NN) N NNNN-NNNN");
        MaskTextWatcher teldpp      = new MaskTextWatcher(txtTelefoneDp, teldp);
        txtTelefoneDp.addTextChangedListener(teldpp);

        SimpleMaskFormatter teldpf  = new SimpleMaskFormatter("(NN) N NNNN-NNNN");
        MaskTextWatcher teldpfm     = new MaskTextWatcher(txtTelFamiliaDp, teldpf);
        txtTelFamiliaDp.addTextChangedListener(teldpfm);

        SimpleMaskFormatter cartSus = new SimpleMaskFormatter("NNNNNNNNNNN NNNN");
        MaskTextWatcher cartSusDp   = new SimpleMaskTextWatcher(txtNumCartaoSusDp, cartSus);
        txtNumCartaoSusDp.addTextChangedListener(cartSusDp);

        SimpleMaskFormatter atl     = new SimpleMaskFormatter("N.NN");
        MaskTextWatcher altr        = new MaskTextWatcher(txtAlturaDp, atl);
        txtAlturaDp.addTextChangedListener(altr);
        //FIM DA MASCARA

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                mDataNascimento.setText(date);
            }
        };

        btnAvencarDp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nome             = txtNomeDp.getText().toString();
                String dataNascimento   = mDataNascimento.getText().toString();
                String telefone         = txtTelefoneDp.getText().toString().replaceAll("[^0-9]", "");
                String telFamilia       = txtTelFamiliaDp.getText().toString().replaceAll("[^0-9]", "");
                String numCartaoSus     = txtNumCartaoSusDp.getText().toString().replaceAll("[^0-9]", "");
                String altura           = txtAlturaDp.getText().toString().replaceAll("[^0-9]", "");
                String peso             = txtPesoDp.getText().toString().replaceAll("[^0-9]", "");
                String doadorOrgao      = txtDoadorOrgaosDp.isChecked() ? "1" : "0";

                if (nome.equals("") || nome.length() <= 8) {
                    msgNomeCompleto();
                    return;
                }
                if (dataNascimento.equals("")) {
                    msgDataNascimento();
                    return;
                }
                if (mySexo.getSelectedItemId() == 0) {
                    msgSexo();
                    return;
                }
                if (telefone.equals("")) {
                    msgTelefone();
                    return;
                }
                if (telefone.length() <= 10){
                    msgTelefoneInvalido();
                    return;
                }
                if (telFamilia.equals("")) {
                    msgContEmerg();
                    return;
                }
                if (telFamilia.length() <= 10) {
                    msgContEmergInvalido();
                    return;
                }
                if (numCartaoSus.length() >= 1 && numCartaoSus.length() <= 14) {
                    msgCataoSusInvalido();
                    return;
                }
                String nomeMaiusculo        = StringUtil.toMaiuscula(nome);
                DadosPessoais dadosPessoais = new DadosPessoais();
                dadosPessoais.setUsers_id(CustomSection.getInstance().getLoggedUser());

                dadosPessoais.setNome(nomeMaiusculo);
                dadosPessoais.setData_nascimento(new Date(dataNascimento));
                dadosPessoais.setTelefone(telefone);
                dadosPessoais.setTel_familiar(telFamilia);
                dadosPessoais.setNum_cartao_sus(numCartaoSus);
                dadosPessoais.setAltura(altura);
                dadosPessoais.setPeso(peso);
                dadosPessoais.setDoador_orgaos(Boolean.parseBoolean(doadorOrgao));
                dadosPessoais.setSexo((Sexo)mySexo.getSelectedItem());

                Intent i = new Intent(DadosPessoaisActivity.this, EnderecoUsersActivity.class);
                i.putExtra("dados", dadosPessoais);
                startActivity(i);
                finish();

            }
        });

        btnCancelarDp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DadosPessoaisActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }
    private void calendar(){
        Calendar c  = Calendar.getInstance();
        int year    = c.get(Calendar.YEAR);
        int month   = c.get(Calendar.MONTH);
        int day     = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                DadosPessoaisActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                year,month,day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    public void onDestroy(){
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    /*mensagem de alerta*/
    public void msgNomeCompleto(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Obrigatório informar Nome Completo.", Style.ALERT);
        crouton.show();
    }
    public void msgDataNascimento(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Obrigatório informar Data de Nascimento.", Style.ALERT);
        crouton.show();
    }

    public void msgSexo(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Obrigatório informar Sexo.", Style.ALERT);
        crouton.show();
    }
    public void msgTelefone(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Obrigatório informar Telefone.", Style.ALERT);
        crouton.show();
    }
    public void msgTelefoneInvalido(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Telefone inválido. Favor informar novamente.", Style.ALERT);
        crouton.show();
    }
    public void msgContEmerg(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Obrigatório informar contato para emergências.", Style.ALERT);
        crouton.show();
    }
    public void msgContEmergInvalido(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Contato para emergências inválido. Favor informar novamente.", Style.ALERT);
        crouton.show();
    }

    public void msgCataoSusInvalido(){
        crouton = Crouton.makeText(DadosPessoaisActivity.this, "Nº cartão SUS inválido. Favor informar novamente.", Style.ALERT);
        crouton.show();
    }
}

