package br.com.helpsocorrista.helpsocorrista.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.helpsocorrista.helpsocorrista.model.TabelaAuxiliar;

public class AuxiliarAdapter extends ArrayAdapter<TabelaAuxiliar> {

    private Context context;
    private List<TabelaAuxiliar> mList;

    public AuxiliarAdapter(Context context, int resource, List<? extends TabelaAuxiliar> objects) {
        super(context, resource, (List<TabelaAuxiliar>) objects);
        this.context = context;
        this.mList = (List<TabelaAuxiliar>) objects;
    }

    public int getCount(){
        return mList.size();
    }

    public TabelaAuxiliar getItem(int position) {
        if (position <= mList.size() - 1
                && position > -1)
            return mList.get(position);
        return null;
    }

    public void setList(List<? extends TabelaAuxiliar > list){
        this.mList = (List<TabelaAuxiliar>) list;
        notifyDataSetChanged();
    }

    @Override
    public int getPosition(TabelaAuxiliar item) {
//        return mList.indexOf(item);
        int pos = 0;
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i) != null && item != null) {
                if (mList.get(i).getId().hashCode() == item.getId().hashCode()) {
                    pos = i;
                    break;
                }
            }
        }
        Log.d("POSICAO", String.valueOf(pos));
        return pos + 1;
    }

    @Override
    public long getItemId(int position) {
        if (mList != null && mList.size() > 0) {
            if (position > mList.size()) {
                return mList.get(mList.size() - 1).getId().hashCode();
            } else if (position == mList.size()) {
                return mList.get(position - 1).getId().hashCode();
            }
            return mList.get(position).getId().hashCode();
        }
        return 0;
    }

    //funcao usada ao criar os itens da lista
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Criado um TextView dinamico, para cada item do Spinner
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        //referencia o TextView com o objeto
        label.setText(mList.get(position).getTextoCurto());
        //retorna a view para cada item do Spinner
        return label;
    }

    public void addItem(TabelaAuxiliar aux) {

        mList.add(aux);
        notifyDataSetChanged();

//        if (mList != null) {
//            if (aux.getId() == null || aux.getId().toString().isEmpty()) {
//                aux.getId(RandomUtil.generateUUID());
//            }
//            mList.add(aux);
//            notifyDataSetChanged();
//        }
    }

    public void updateItem(TabelaAuxiliar aux) {
        if (aux != null) {
            if (mList != null) {
                for (int i = 0; i < mList.size(); i++) {
                    if (mList.get(i).getId() == aux.getId()) {
                        mList.set(i, aux);
                        notifyDataSetChanged();
                        return;
                    }
                }
            }
        }
    }

    public void removeFromListById(Integer id) {
        if (mList != null) {
            for (TabelaAuxiliar is : mList) {
                if (is.getId() == id) {
                    mList.remove(is);
                    notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    //funcao chamada quando o "chooser" é selecionado
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        label.setPadding(5,5,5,5);
        Object o = mList.get(position);
        label.setText(mList.get(position).getTextoCurto());
        return label;
    }

    public List<TabelaAuxiliar> getList() {
        return mList;
    }

}
