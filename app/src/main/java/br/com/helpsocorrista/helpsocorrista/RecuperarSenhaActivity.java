package br.com.helpsocorrista.helpsocorrista;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.Helpers.ValidaCPF;
import br.com.helpsocorrista.helpsocorrista.dao.DadosPessoaisDAO;
import br.com.helpsocorrista.helpsocorrista.dao.UsersDAO;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;
import br.com.helpsocorrista.helpsocorrista.model.Users;

public class RecuperarSenhaActivity extends AppCompatActivity {

    private EditText txtRecCpf;
    private EditText txtRecTelef;
    private TextView txtNovaSenha;
    private Button   btnRecSenha;
    private Button   btnCancelarRecSenha;

    UsersDAO usersDAO   = new UsersDAO();
    Users usuario       = new Users();


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar__senha);

        ActivityCompat.requestPermissions(this, new String[]{
            Manifest.permission.SEND_SMS
        }, 1);

        txtRecCpf           = (EditText) findViewById(R.id.txtRecCpf);
        txtRecTelef         = (EditText) findViewById(R.id.txtRecTelef);
        txtNovaSenha        = (TextView) findViewById(R.id.txtNovaSenha);
        btnRecSenha         = (Button) findViewById(R.id.btnRecSenha);
        btnCancelarRecSenha = (Button) findViewById(R.id.btnCancelarRecSenha);


        //INICIO DA MASCARA
        SimpleMaskFormatter smfCpf = new SimpleMaskFormatter("NNN.NNN.NNN-NN");
        MaskTextWatcher mtwCpf     = new MaskTextWatcher(txtRecCpf, smfCpf);
        txtRecCpf.addTextChangedListener(mtwCpf);

        SimpleMaskFormatter smfTel = new SimpleMaskFormatter("(NN) N NNNN-NNNN");
        MaskTextWatcher mtwTel = new MaskTextWatcher(txtRecTelef, smfTel);
        txtRecTelef.addTextChangedListener(mtwTel);
        //FIM DA MASCARA

        btnRecSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public
            void onClick(View v) {



                String numTelefone  = txtRecTelef.getText().toString();
                String novaSenha    = txtNovaSenha.getText().toString();
//

//
                String cpf      = String.valueOf(txtRecCpf.getText().toString().replaceAll("[^0-9]", ""));
                String telef    = txtRecTelef.getText().toString().replaceAll("[^0-9]", "");

//                if (cpf.equals("")){
//                        txtRecCpf.setError("Obrigatório informar CPF");
////                    Toast.makeText(getApplicationContext(), "Obrigatório informar CPF", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                if (telef.equals("")){
//                    Toast.makeText(getApplicationContext(), "Obrigatório informar Telefone", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                if (telef.length() <= 10){
//                    Toast.makeText(getApplicationContext(), "Telefone inválido. Favor informar navamente", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                if (!ValidaCPF.isCPF(cpf)){
//                    Toast.makeText(getApplicationContext(),"CPF inválido. Favor informar novamente.", Toast.LENGTH_LONG).show();
//                }
//                else{Toast.makeText(getApplicationContext(),"Nova Senha enviada", Toast.LENGTH_LONG).show();
//                }

                UsersDAO usersDAO = new UsersDAO();
                Users usuarioUnico = null;
//                Users novaSenha = null;

                DadosPessoaisDAO dadosPessoaisDAO = new DadosPessoaisDAO();
                DadosPessoais telefUnico = null;

                try {
                    usuarioUnico = usersDAO.getCpf(cpf);
                    telefUnico = dadosPessoaisDAO.getTelefone(telef);
//                    novaSenha = usersDAO.getSenhaMobile(novaSenha);

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (usuarioUnico != null && telefUnico != null) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(numTelefone, null, novaSenha, null, null);
                    Toast.makeText(getApplicationContext(),"Senha Enviada.", Toast.LENGTH_LONG).show();
                    return;
                }
                else {
                    Toast.makeText(getApplicationContext(), "CFP ou Telefone Inválido.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });

        btnCancelarRecSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
