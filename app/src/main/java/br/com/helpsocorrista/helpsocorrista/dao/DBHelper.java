package br.com.helpsocorrista.helpsocorrista.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.helpsocorrista.helpsocorrista.R;
import br.com.helpsocorrista.helpsocorrista.model.Alergias;
import br.com.helpsocorrista.helpsocorrista.model.AlergiasUsers;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;
import br.com.helpsocorrista.helpsocorrista.model.Doencas;
import br.com.helpsocorrista.helpsocorrista.model.DoencasUsers;
import br.com.helpsocorrista.helpsocorrista.model.EndSolicitacao;
import br.com.helpsocorrista.helpsocorrista.model.ImagemUser;
import br.com.helpsocorrista.helpsocorrista.model.MedicamControlados;
import br.com.helpsocorrista.helpsocorrista.model.MedicamControladosUsers;
import br.com.helpsocorrista.helpsocorrista.model.Solicitacao;
import br.com.helpsocorrista.helpsocorrista.model.StatusSolicitacao;
import br.com.helpsocorrista.helpsocorrista.model.Users;

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "dbhelp";
    private static String DATABASE_PATH ="";
    private static final int DATABASE_VERSION = 1;

    private static DBHelper mInstance;
    private static Context mContext;

    public static DBHelper getmInstance(){
        if (mInstance == null) {
            mInstance = new DBHelper(mContext.getApplicationContext());
        }
        return mInstance;
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
        mContext = context;
        DATABASE_PATH = context.getApplicationInfo().dataDir + File.separator + "databases" + File.separator;
    }


    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Alergias.class);
            TableUtils.createTable(connectionSource, Users.class);
            TableUtils.createTable(connectionSource, AlergiasUsers.class);
            TableUtils.createTable(connectionSource, Doencas.class);
            TableUtils.createTable(connectionSource, DoencasUsers.class);
            TableUtils.createTable(connectionSource, MedicamControlados.class);
            TableUtils.createTable(connectionSource, MedicamControladosUsers.class);
            TableUtils.createTable(connectionSource, EndSolicitacao.class);
            TableUtils.createTable(connectionSource, StatusSolicitacao.class);
            TableUtils.createTable(connectionSource, Solicitacao.class);
            TableUtils.createTable(connectionSource, DadosPessoais.class);
            TableUtils.createTable(connectionSource, ImagemUser.class);


        } catch (java.sql.SQLException e) {
            Log.e(DBHelper.class.getName(), "Erro ao criar o banco de dados", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public void relaseHelper() {
        OpenHelperManager.releaseHelper();
    }

    @Override
    public void close(){
        super.close();
    }



    public static String exportExternalDataBase(String path) throws IOException {
        String APPDATA = path + File.separator;

        if (path == null || "".equals(path)) {
            String SDCARD_DIR = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + File.separator;
            APPDATA = SDCARD_DIR + "MY_APP" + File.separator;
        }

        File folder = new File(APPDATA);
        if (!folder.exists())
            folder.mkdir();

        String inFileName = DATABASE_PATH + DATABASE_NAME;
        InputStream mInput = new FileInputStream(inFileName);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String finalName = APPDATA + DATABASE_NAME + "-" + timeStamp;
        OutputStream mOutput = new FileOutputStream(finalName);

        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }

        mOutput.flush();
        mOutput.close();
        mInput.close();

        return APPDATA;
    }

}