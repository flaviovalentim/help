package br.com.helpsocorrista.helpsocorrista;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.dao.DadosPessoaisDAO;
import br.com.helpsocorrista.helpsocorrista.dao.ImagemUserDAO;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;
import br.com.helpsocorrista.helpsocorrista.model.ImagemUser;
import br.com.helpsocorrista.helpsocorrista.model.Users;
import br.com.helpsocorrista.helpsocorrista.util.CustomSection;
import br.com.helpsocorrista.helpsocorrista.util.FileUtil;
import br.com.helpsocorrista.helpsocorrista.util.ImagemUtil;
import de.hdodenhof.circleimageview.CircleImageView;

public class ImagemUserActivity extends AppCompatActivity {

    public static final int REQUEST_TAKE_PHOTO = 5;
    public static final int PICK_IMAGE_GALLERY_REQUEST = 6;

    private CircleImageView txtCapImagem;
    private TextView        txtNomeUsers;
    private Button          btnTirarFoto;
    private String          mCurrentPhotoPath;
    private ImagemUser      imagemUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagem_user);

        txtCapImagem = (CircleImageView) findViewById(R.id.txtCapImagem);
        txtNomeUsers = (TextView)findViewById(R.id.txtNomeUsers);

        Users usuarioLogado = CustomSection.getInstance().getLoggedUser();
        if (usuarioLogado != null) {
            try {
                imagemUser                  = new ImagemUserDAO().getByUserId(usuarioLogado.getId());
                DadosPessoais dadosPessoais = new DadosPessoaisDAO().getByUserId(usuarioLogado.getId());
                if (imagemUser != null && imagemUser.getCaminho() != null) {
                    mCurrentPhotoPath = imagemUser.getCaminho();
                    txtNomeUsers.setText(dadosPessoais.getNome());
                    configureView();
                }else {
                    txtNomeUsers.setText(dadosPessoais.getNome());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(imagemUser == null){
            imagemUser = new ImagemUser();
        }

    }

    public void selectOptionsImage(View v){
        new AlertDialog.Builder(this)
                       .setIcon(R.drawable.ic_menu_camera)
                       .setTitle("Selecione uma opção")
                       .setNegativeButton("Galeria", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                               importImageFromGallery();
                           }
                       })
                       .setPositiveButton("Câmera", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                               importImageFromCamera();
                           }
                       })
                       .show();
    }

    private void importImageFromGallery(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Selecione uma imagem"), PICK_IMAGE_GALLERY_REQUEST);
    }

    private void importImageFromCamera(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(ImagemUserActivity.this.getPackageManager()) != null) {
            File photoFile = null;

            try {
                photoFile = ImagemUtil.createImageFileProfile();
                mCurrentPhotoPath = photoFile.getPath();

                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            switch(requestCode){
                case REQUEST_TAKE_PHOTO:

                    if(data != null && data.getData() != null){
                        mCurrentPhotoPath =  data.getData().toString();
                    }
                    saveImage();
                    configureView();

                    break;
                case PICK_IMAGE_GALLERY_REQUEST:
                    if (data != null && data.getData() != null) {

                        Uri uriPhotoFIle = data.getData();
                        File file = new File(FileUtil.getRealPathFromURI(ImagemUserActivity.this, uriPhotoFIle));

                        if (file != null && file.getPath() != null) {
                            mCurrentPhotoPath = file.getPath();
                        }
                        saveImage();
                        configureView();
                    }
                    break;
            }
        }
    }

    private void configureView(){
        File file = new File(mCurrentPhotoPath);
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

        //ABRIR NA GALERIA
        if (bitmap != null) {
            txtCapImagem.setImageBitmap(bitmap);
            txtCapImagem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(ImagemUserActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("O que deseja?")
                            .setMessage("Visualizar a imagem na galeria?")
                            .setPositiveButton("Visualizar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ImagemUtil.openImagemInGallery(ImagemUserActivity.this, mCurrentPhotoPath);
                                }
                            })
                            .show();
                }
            });
        }
    }

    private void saveImage(){
        //salvar para enviar para a view
        imagemUser.setUsers_id(CustomSection.getInstance().getLoggedUser());
        imagemUser.setCaminho(mCurrentPhotoPath);
        try {
            if (imagemUser != null) {
                new ImagemUserDAO().save(imagemUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed(){
        Intent i = new Intent(ImagemUserActivity.this, SolicitacaoActivity.class);
        startActivity(i);
        finish();
    }
}

