package br.com.helpsocorrista.helpsocorrista.model;

import android.renderscript.Element;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

import br.com.helpsocorrista.helpsocorrista.Helpers.enums.Sexo;
import br.com.helpsocorrista.helpsocorrista.Helpers.enums.TipoSanguineo;

@DatabaseTable(tableName = DadosPessoais.TABLE_NAME)
public class DadosPessoais implements Serializable {
    public static final String TABLE_NAME               = "dados_pessoais";
    public static final String COLUMN_ID                = "id";
    public static final String COLUMN_NOME              = "nome";
    public static final String COLUMN_SEXO              = "sexo";
    public static final String COLUMN_TIPO_SANGUINEO    = "tipo_Sanguineo";
    public static final String COLUMN_DATA_NASCIMENTO   = "data_nascimento";
    public static final String COLUMN_TELEFONE          = "telefone";
    public static final String COLUMN_TEL_FAMILIAR      = "tel_familiar";
    public static final String COLUMN_NUM_CARTAO_SUS    = "num_cartao_sus";
    public static final String COLUMN_LOGRADOURO        = "logradouro";
    public static final String COLUMN_NUMERO            = "numero";
    public static final String COLUMN_CEP               = "cep";
    public static final String COLUMN_BAIRRO            = "bairro";
    public static final String COLUMN_COMPLEMENTO       = "complemento";
    public static final String COLUMN_PONTO_REFERENCIA  = "ponto_referencia";
    public static final String COLUMN_DOADOR_ORGAOS     = "doador_orgaos";
    public static final String COLUMN_ALTURA            = "altura";
    public static final String COLUMN_PESO              = "peso";
    public static final String COLUMN_LATITUDE          = "latitude";
    public static final String COLUMN_LONGITUDE         = "longitude";
    public static final String COLUMN_USERS_ID          = "users_id";

    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_NOME)
    private String nome;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_SEXO)
    private Sexo sexo;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_TIPO_SANGUINEO)
    private TipoSanguineo tipoSanguineo;

    @Expose
    @DatabaseField(canBeNull = false, dataType = DataType.DATE_STRING, format = "yyyy-MM-dd", columnName = COLUMN_DATA_NASCIMENTO)
    private Date data_nascimento;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_TELEFONE)
    private String telefone;

    @Expose
    @DatabaseField(columnName = COLUMN_TEL_FAMILIAR)
    private String tel_familiar;

    @Expose
    @DatabaseField(columnName = COLUMN_NUM_CARTAO_SUS)
    private String num_cartao_sus;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_LOGRADOURO)
    private String logradouro;

    @Expose
    @DatabaseField(columnName = COLUMN_NUMERO)
    private int numero;

    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_CEP)
    private int cep;

    @Expose
    @DatabaseField(columnName = COLUMN_BAIRRO)
    private String bairro;

    @Expose
    @DatabaseField(columnName = COLUMN_COMPLEMENTO)
    private String complemento;

    @Expose
    @DatabaseField(columnName = COLUMN_PONTO_REFERENCIA)
    private String ponto_referencia;

    @Expose
    @DatabaseField(columnName = COLUMN_DOADOR_ORGAOS)
    private boolean doador_orgaos;

    @Expose
    @DatabaseField(columnName = COLUMN_ALTURA)
    private String altura;

    @Expose
    @DatabaseField(columnName = COLUMN_PESO)
    private String peso;

    @Expose
    @DatabaseField(columnName = COLUMN_LATITUDE)
    private String latitude;

    @Expose
    @DatabaseField(columnName = COLUMN_LONGITUDE)
    private String longitude;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_ID)
    private Users users_id;


    public DadosPessoais(){}

    // Getter and Setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public TipoSanguineo getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTel_familiar() {
        return tel_familiar;
    }

    public void setTel_familiar(String tel_familiar) {
        this.tel_familiar = tel_familiar;
    }

    public String getNum_cartao_sus() {
        return num_cartao_sus;
    }

    public void setNum_cartao_sus(String num_cartao_sus) {
        this.num_cartao_sus = num_cartao_sus;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numeto) {
        this.numero = numeto;
    }

    public int getCep() {
        return cep;
    }

    public void setCep(int cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getPonto_referencia() {
        return ponto_referencia;
    }

    public void setPonto_referencia(String ponto_referencia) {
        this.ponto_referencia = ponto_referencia;
    }

    public boolean isDoador_orgaos() {
        return doador_orgaos;
    }

    public void setDoador_orgaos(boolean doador_orgaos) {
        this.doador_orgaos = doador_orgaos;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }
}
