package br.com.helpsocorrista.helpsocorrista.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

import br.com.helpsocorrista.helpsocorrista.Helpers.enums.GrauUrgencia;


@DatabaseTable(tableName = Solicitacao.TABLE_NAME)
public class Solicitacao implements Parcelable, Serializable {

    public static final String TABLE_NAME                   = "solicitacao";
    public static final String COLUMN_ID                    = "id";
    public static final String COLUMN_NUM_OCORRENCIA        = "num_ocorrencia";
    public static final String COLUMN_DATA_INICIO           = "data_inicio";
    public static final String COLUMN_DATA_TERMINO          = "data_termino";
    public static final String COLUMN_DESCRICAO_SOLIC       = "descricao_solicitacao";
    public static final String COLUMN_GRAU_URGENCIA         = "grau_de_urgencia";
    public static final String COLUMN_STATUS_SOLICITACAO    = "status_solicitacao";
    public static final String COLUMN_USERS_ID              = "users_id";
//    public static final String COLUMN_END_SOLICITACAO_ID    = "end_solicitacao_id";
//    public static final String COLUMN_USERS_SOLICITACAO_ID  = "users_solicitacao_id";
//    public static final String COLUMN_STATUS_SOLICITACAO_ID = "status_solicitacao_id";

    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(columnName = COLUMN_NUM_OCORRENCIA)
    private String num_ocorrencia;

    @Expose
    @DatabaseField(columnName = COLUMN_DATA_INICIO)
    private Date data_inicio;

    @Expose
    @DatabaseField(columnName = COLUMN_DATA_TERMINO)
    private Date data_termino;

    @Expose
    @DatabaseField(columnName = COLUMN_DESCRICAO_SOLIC)
    private String descricao_solicitacao;

    @Expose
    @DatabaseField(columnName = COLUMN_GRAU_URGENCIA)
    private String grau_de_urgencia;

    @Expose
    @DatabaseField(columnName = COLUMN_STATUS_SOLICITACAO)
    private String statusSolicitacao;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_ID)
    private Users users_id;

//    @Expose
//    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_END_SOLICITACAO_ID)
//    private EndSolicitacao endSolicitacao_id;
//
//    @Expose
//    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_SOLICITACAO_ID)
//    private Users users_solicitacao_id;
//
//    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_STATUS_SOLICITACAO_ID)
//    private StatusSolicitacao status_solicitacao_id;


    //construtor vazio
    public Solicitacao(){}

    // Getter and Setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNum_ocorrencia() {
        return num_ocorrencia;
    }

    public void setNum_cocorrencia(String num_cocorrencia) {
        this.num_ocorrencia = num_cocorrencia;
    }

//    public StatusSolicitacao getStatus_solicitacao_id() {
//        return status_solicitacao_id;
//    }
//
//    public void setStatus_solicitacao_id(StatusSolicitacao status_solicitacao_id) {
//        this.status_solicitacao_id = status_solicitacao_id;
//    }

    public Date getData_inicio() {
        return data_inicio;
    }

    public void setData_inicio(Date data_inicio) {
        this.data_inicio = data_inicio;
    }

    public Date getData_termino() {
        return data_termino;
    }

    public void setData_termino(Date data_termino) {
        this.data_termino = data_termino;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }

//    public EndSolicitacao getEndSolicitacao_id() {
//        return endSolicitacao_id;
//    }
//
//    public void setEndSolicitacao_id(EndSolicitacao endSolicitacao_id) {
//        this.endSolicitacao_id = endSolicitacao_id;
//    }
//
//    public Users getUsers_solicitacao_id() {
//        return users_solicitacao_id;
//    }
//
//    public void setUsers_solicitacao_id(Users users_solicitacao_id) {
//        this.users_solicitacao_id = users_solicitacao_id;
//    }


    public void setNum_ocorrencia(String num_ocorrencia) {
        this.num_ocorrencia = num_ocorrencia;
    }

    public String getDescricao_solicitacao() {
        return descricao_solicitacao;
    }

    public void setDescricao_solicitacao(String descricao_solicitacao) {
        this.descricao_solicitacao = descricao_solicitacao;
    }

    public String getGrau_de_urgencia() {
        return grau_de_urgencia;
    }

    public void setGrau_de_urgencia(String grau_de_urgencia) {
        this.grau_de_urgencia = grau_de_urgencia;
    }

    public String getStatusSolicitacao() {
        return statusSolicitacao;
    }

    public void setStatusSolicitacao(String statusSolicitacao) {
        this.statusSolicitacao = statusSolicitacao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeString(String.valueOf(this.data_inicio !=null ? this.data_inicio.getTime() : -1));
        dest.writeString(String.valueOf(this.data_termino !=null ? this.data_termino.getTime() : -1));
//        dest.writeParcelable(this.status_solicitacao_id, flags);
        dest.writeParcelable(this.users_id, flags);
//        dest.writeParcelable(this.endSolicitacao_id, flags);
//        dest.writeParcelable(this.users_solicitacao_id, flags);

        dest.writeString(this.descricao_solicitacao);
        dest.writeString(this.grau_de_urgencia);
        dest.writeString(this.statusSolicitacao);
    }

    protected Solicitacao(Parcel in) {
        this.id = (Integer) in.readSerializable();
        long tmpData_inicio = in.readLong();
        this.data_inicio = tmpData_inicio == -1 ? null : new Date(tmpData_inicio);
        long tmpData_termino = in.readLong();
        this.data_termino = tmpData_termino == -1 ? null : new Date(tmpData_termino);
//        this.status_solicitacao_id = (StatusSolicitacao) in.readSerializable();
        this.users_id = in.readParcelable(Users.class.getClassLoader());
//        this.endSolicitacao_id = in.readParcelable(EndSolicitacao.class.getClassLoader());
//        this.users_solicitacao_id = in.readParcelable(Users.class.getClassLoader());

        this.descricao_solicitacao  = in.readString();
        this.grau_de_urgencia       = in.readString();
        this.statusSolicitacao      = in.readString();
    }

    public static Creator<Solicitacao> CREATOR = new Creator<Solicitacao>() {
        @Override
        public Solicitacao createFromParcel(Parcel source) {
            return new Solicitacao(source);
        }

        @Override
        public Solicitacao[] newArray(int size) {
            return new Solicitacao[size];
        }
    };


}
