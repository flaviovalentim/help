package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.model.ImagemUser;

public class ImagemUserDAO {

    private Dao<ImagemUser, Integer> dao;

    private Dao<ImagemUser, Integer> getDao() throws SQLException{
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(ImagemUser.class);
        }
        return dao;
    }

    public void save(ImagemUser imagemUser) throws SQLException {
        if (imagemUser.getId() == null || imagemUser.getId() == 0) {
            getDao().create(imagemUser);
        } else {
            if (getById(imagemUser.getId()) == null) {
                getDao().create(imagemUser);
            } else {
                getDao().update(imagemUser);
            }
        }
    }

    private ImagemUser getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

    public ImagemUser getByUserId(Integer id) throws SQLException {
        return getDao().queryBuilder()
                .where()
                .eq(ImagemUser.COLUMN_USERS_ID, id).queryForFirst();
    }

}
