package br.com.helpsocorrista.helpsocorrista.dao;


import com.j256.ormlite.dao.Dao;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.helpsocorrista.helpsocorrista.model.Alergias;

public class AlergiasDAO {

    private Dao<Alergias, Integer> dao;

    private Dao<Alergias, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(Alergias.class);
        }
        return dao;
    }

    public void save(Alergias alergias) throws SQLException{
        if(alergias.getId() == null || alergias.getId() == 0){
            getDao().create(alergias);
        } else {
            if (getById(alergias.getId()) == null) {
                getDao().create(alergias);
            } else {
                getDao().update(alergias);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public Alergias getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

    public List<Alergias> getAllToSpinner() throws SQLException {
        List<Alergias> alergias = new ArrayList<>();
        alergias.add(new Alergias(-1, "Selecione"));
        alergias.addAll(getDao().queryBuilder().orderBy(Alergias.COLUMN_DESCRICAO, true).query());

        return alergias;
    }
}

