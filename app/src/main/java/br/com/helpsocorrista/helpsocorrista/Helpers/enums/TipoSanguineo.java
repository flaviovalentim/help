package br.com.helpsocorrista.helpsocorrista.Helpers.enums;

public enum TipoSanguineo {
    Selecione("Selecione"),
    Sem_Informação("Não sei Informar"),
    A_Positivo("A+"),
    A_Negativo("A-"),
    B_Positivo("B+"),
    B_Negativo("B-"),
    AB_Positivo("AB+"),
    AB_Negativo("AB-"),
    O_Positivo("O+"),
    O_Negativo("O-");

    private String tipoSaguineo;

    private TipoSanguineo(String tipoSaguineo){
        this.tipoSaguineo = tipoSaguineo;
    }

    @Override
    public String toString() {
        return tipoSaguineo;
    }
}
