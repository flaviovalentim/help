package br.com.helpsocorrista.helpsocorrista.Services.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import br.com.helpsocorrista.helpsocorrista.model.MedicamControlados;

public class MedicamentoDto {

    @SerializedName("current_page")
    private Integer current_page;

    @SerializedName("data")
    ArrayList<MedicamControlados> date;

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public ArrayList<MedicamControlados> getDate() {
        return date;
    }

    public void setDate(ArrayList<MedicamControlados> date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MedicamentoDto{" +
                "current_page=" + current_page +
                ", date=" + date +
                '}';
    }
}
