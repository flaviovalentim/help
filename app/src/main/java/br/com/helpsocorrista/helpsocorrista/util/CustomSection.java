package br.com.helpsocorrista.helpsocorrista.util;

import br.com.helpsocorrista.helpsocorrista.model.Users;

public class CustomSection {
    private static CustomSection section = null;
    private Users loggedUser;
    private Users loggedUserReset;

    public static CustomSection getInstance(){
        if(section == null){
            section = new CustomSection();
        }
        return section;
    }

    public Users getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(Users loggedUser) {
        this.loggedUser = loggedUser;
    }

    public Users getLoggedUserReset(Users usurioReset) {
        return loggedUserReset;
    }

    public void setLoggedUserReset(Users loggedUserReset) {
        this.loggedUserReset = loggedUserReset;
    }
}
