package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.model.Users;

import static android.service.autofill.Validators.and;

public class UsersDAO {
    private Dao<Users, Integer> dao;

    private Dao<Users, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(Users.class);
        }
        return dao;
    }

    public void save(Users users) throws SQLException {
        if (users.getId() == null || users.getId().toString().isEmpty()) {
            getDao().create(users);
        }
        else {
            if (getById(users.getId()) == null) {
                getDao().create(users);
            } else {
                getDao().update(users);
            }
        }
        DBHelper.getmInstance().relaseHelper();
    }

    public Users getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

    public Users getByCredentials(String cpf, String password ) throws SQLException {

        return getDao().queryBuilder()
                .where()
                .eq(Users.COLUMN_CPF, cpf)
                .and()
                .eq(Users.COLUMN_SENHA_MOBILE, password).queryForFirst();
    }

    public Users getByResetCredentials(String cpf, String senhaReset) throws SQLException {
        return getDao().queryBuilder()
                .where()
                .eq(Users.COLUMN_CPF, cpf)
                .and()
                .eq(Users.COLUMN_NOVA_SENHA, senhaReset).queryForFirst();

    }

    public Users getCpf(String cpf) throws SQLException {
        return getDao().queryBuilder()
                .where()
                .eq(Users.COLUMN_CPF, cpf).queryForFirst();
    }

    public Users getSenhaReset(String senhaReset) throws SQLException {
        return getDao().queryBuilder()
                .where()
                .eq(Users.COLUMN_NOVA_SENHA, senhaReset).queryForFirst();
    }

}
