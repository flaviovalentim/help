package br.com.helpsocorrista.helpsocorrista.Helpers.enums;

public enum Sexo {
    S("Selecione"),
    M("Masculino"),
    F("Feminino"),
    OT("Outro"),
    NI("Não Informado");

    private String descricao;

    private Sexo(String descricao){
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
