package br.com.helpsocorrista.helpsocorrista.Services.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


import br.com.helpsocorrista.helpsocorrista.model.Alergias;

public class AlergiaDto {
    @SerializedName("current_page")
    private Integer current_page;

    @SerializedName("data")
    ArrayList<Alergias> data;

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public ArrayList<Alergias> getData() {
        return data;
    }

    public void setData(ArrayList<Alergias> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "AlergiaDto{" +
                "current_page=" + current_page +
                ", data=" + data +
                '}';
    }
}
