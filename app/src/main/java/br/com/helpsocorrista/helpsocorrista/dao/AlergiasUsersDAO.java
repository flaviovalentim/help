package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.model.AlergiasUsers;

public class AlergiasUsersDAO {

    private Dao<AlergiasUsers, Integer> dao;

    private Dao<AlergiasUsers, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(AlergiasUsers.class);
        }
        return dao;
    }

    public void save(AlergiasUsers alergiasUsers) throws SQLException{
        if(alergiasUsers.getId() == null || alergiasUsers.getId() == 0){
            getDao().create(alergiasUsers);
        } else {
            if (getById(alergiasUsers.getId()) == null) {
                getDao().create(alergiasUsers);
            } else {
                getDao().update(alergiasUsers);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public AlergiasUsers getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

}
