package br.com.helpsocorrista.helpsocorrista.model;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = ImagemUser.TABLE_NAME)
public class ImagemUser implements Serializable {
    public static final String TABLE_NAME = "imagem_User";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CAMINHO = "caminho";
    public static final String COLUMN_USERS_ID = "users_id";

    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;

    @Expose
    @DatabaseField(columnName = COLUMN_CAMINHO)
    private String caminho;

    @Expose
    @DatabaseField(foreign = true, canBeNull = false, columnName = COLUMN_USERS_ID)
    private Users users_id;

    public ImagemUser(){}

    // Getter and Setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public Users getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Users users_id) {
        this.users_id = users_id;
    }


}
