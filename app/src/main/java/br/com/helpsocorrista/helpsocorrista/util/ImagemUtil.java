package br.com.helpsocorrista.helpsocorrista.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class ImagemUtil {
    public static final String PATH_DEFAULT = Environment.DIRECTORY_PICTURES + File.separator + "HELP_SOCORRISTA" + File.separator;

    public static File createImageFileProfile(){
        try {

            String nameRamdom = UUID.randomUUID().toString();

            return storeImageFile(PATH_DEFAULT + "PROFILE_USER" + File.separator, nameRamdom);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static File storeImageFile(String path, String name) throws IOException {
        File dir = new File(path);

        File storageDir = Environment.getExternalStoragePublicDirectory(path);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        String imageFileName = name + ".jpg";
        File image = new File(storageDir, imageFileName);

        return image;
    }

    public static void openImagemInGallery(final Context context, final String path) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + path), "image/*");
        context.startActivity(intent);
    }
}
