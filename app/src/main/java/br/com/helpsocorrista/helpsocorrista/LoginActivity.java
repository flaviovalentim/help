package br.com.helpsocorrista.helpsocorrista;


import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import br.com.helpsocorrista.helpsocorrista.Services.Auth.AccessToken;
import br.com.helpsocorrista.helpsocorrista.Services.Interfaces.ServiceLogin;
import br.com.helpsocorrista.helpsocorrista.Services.RetrofitClientInstance;
import br.com.helpsocorrista.helpsocorrista.Services.Sync;
import br.com.helpsocorrista.helpsocorrista.dao.DBHelper;
import br.com.helpsocorrista.helpsocorrista.Helpers.ValidaCPF;
import br.com.helpsocorrista.helpsocorrista.dao.DadosPessoaisDAO;
import br.com.helpsocorrista.helpsocorrista.dao.UsersDAO;
import br.com.helpsocorrista.helpsocorrista.model.Users;
import br.com.helpsocorrista.helpsocorrista.util.CustomSection;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText    txtCpf;
    private EditText    txtSenha;
    private Button      btnEntrar;
    private Button      btnRecuperarSenha;
    private Crouton     crouton;
    Users               usuario;
    Users               usurioReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtCpf              = (EditText)findViewById(R.id.txtCpf);
        txtSenha            = (EditText)findViewById(R.id.txtSenha);
        btnEntrar           = (Button)findViewById(R.id.btnEntrar);
        btnRecuperarSenha   = (Button)findViewById(R.id.btnRecuperarSenha);

        //INICIO DA MASCARA
        SimpleMaskFormatter smf = new SimpleMaskFormatter("NNN.NNN.NNN-NN");
        MaskTextWatcher mtw     = new MaskTextWatcher(txtCpf, smf);
        txtCpf.addTextChangedListener(mtw);
        //FIM DA MASCARA

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtCpf          = (EditText)findViewById(R.id.txtCpf);
                txtSenha        = (EditText)findViewById(R.id.txtSenha);
                String cpf      = txtCpf.getText().toString().replaceAll("[^0-9]", "");
                String senha    = txtSenha.getText().toString();
                try {
                    Sync.syncDoencas();
                    Sync.syncAlergias();
                    Sync.syncMedicamControlados();

                    UsersDAO usersDAO   = new UsersDAO();
                    usuario             = usersDAO.getByCredentials(cpf, senha);
                    usurioReset         = usersDAO.getByResetCredentials(cpf, senha);

                    if (usuario != null){

                        CustomSection ss = CustomSection.getInstance();
                        ss.setLoggedUser(usuario);

                        if(new DadosPessoaisDAO().getByUserId(usuario.getId()) != null){
                            Intent i = new Intent(getApplicationContext(),SolicitacaoActivity.class);
                            startActivity(i);
                        }else{
                            Intent i = new Intent(getApplicationContext(),DadosPessoaisActivity.class);
                            startActivity(i);
                        }
                        finish();
                    } else if (usurioReset != null){

                        CustomSection cs = CustomSection.getInstance();
                        cs.setLoggedUserReset(usurioReset);

                        if (new DadosPessoaisDAO().getByUserId(usurioReset.getId()) != null){
                            Intent i = new Intent(getApplicationContext(), DadosPessoaisActivity.class);
                            startActivity(i);
                        }else{
                            Intent i = new Intent(getApplicationContext(),DadosPessoaisActivity.class);
                            startActivity(i);
                        }
                        finish();
                    }

                    else if (cpf.equals("") && senha.equals("")){
                        msgCpfSenhaVazio();
                        return;                        }
                    else if(senha.equals("")) {
                        msgSenha();
                        return;
                    }
                    else if(senha.length() <= 5) {
                        msgTamanhoSenha();
                        return;
                    }
                    else if (cpf.equals("")){
                        msgCpfVazio();
                        return;
                    }
                    else if(!ValidaCPF.isCPF(cpf)){
                        msgCpfIvalido();
                        return;
                    }
                    else{
                        msgCredenciasInvalidas();
                        txtCpf.findFocus();
                        return;
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (java.sql.SQLException e) {
                    e.printStackTrace();
                }
//                usuario.setCpf("03374222498");
//                usuario.setSenhaMobile("123456");
//                JSONObject usuarioOBJ = new JSONObject();
//                try {
//                    usuarioOBJ.put("username", "victorfernandes.matias@gmail.com");
//                    usuarioOBJ.put("password", "turismo123");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

//                ServiceLogin service = RetrofitClientInstance.getRetrofitInstance().create(ServiceLogin.class);
//                Call<AccessToken> call = service.login(usuario);
//                call.enqueue(new Callback<AccessToken>() {
//                    @Override
//                    public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
//                        LoginActivity.this.usuario.setAccessTokenn(response.body());
//                        Toast.makeText(LoginActivity.this, usuario.getAccessToken().getToken(),Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void onFailure(Call<AccessToken> call, Throwable t) {
//                        Toast.makeText(LoginActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
//                    }
//                });
            }
        });

        btnRecuperarSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RecuperarSenhaActivity.class );
                startActivity(i);
                finish();
            }
        });

    }
    public void onDestroy(){
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }
    /*mensagem de alerta*/
    public void msgCpfSenhaVazio(){
        crouton = Crouton.makeText(LoginActivity.this, "Obrigatório informar CPF.", Style.ALERT);
        crouton.show();
    }
    public void msgCpfVazio(){
        crouton = Crouton.makeText(LoginActivity.this, "Obrigatório informar CPF.", Style.ALERT);
        crouton.show();
    }
    public void msgSenha(){
        crouton = Crouton.makeText(LoginActivity.this, "Obrigatório informar Senha.", Style.ALERT);
        crouton.show();
    }
    public void msgTamanhoSenha(){
        crouton = Crouton.makeText(LoginActivity.this, "Senha deve conter 6 dígitos.", Style.ALERT);
        crouton.show();
    }
    public void msgCpfIvalido(){
        crouton = Crouton.makeText(LoginActivity.this, "CPF inválido. Favor informar novamente.", Style.ALERT);
        crouton.show();
    }
    public void msgCredenciasInvalidas(){
        crouton = Crouton.makeText(LoginActivity.this, "Credenciais inválidas.", Style.ALERT);
        crouton.show();
    }
}


