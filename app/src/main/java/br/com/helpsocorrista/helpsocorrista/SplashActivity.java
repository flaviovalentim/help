package br.com.helpsocorrista.helpsocorrista;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.dao.AlergiasDAO;
import br.com.helpsocorrista.helpsocorrista.dao.AlergiasUsersDAO;
import br.com.helpsocorrista.helpsocorrista.dao.DBHelper;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class SplashActivity extends AppCompatActivity {
    private Crouton crouton;

    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, PrincipalActivity.class);
                    finish();
                    startActivity(i);
                }
            }, SPLASH_TIME_OUT);

            try {
                new DBHelper(this);
                new AlergiasDAO().getAllToSpinner();
                DBHelper.exportExternalDataBase("");
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            crouton = Crouton.makeText(SplashActivity.this, "Dispositivo sem conecxão com a internet", Style.INFO);
            crouton.show();
            return;
        }


    }
    public void onDestroy(){
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }
}
