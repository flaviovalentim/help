package br.com.helpsocorrista.helpsocorrista.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import br.com.helpsocorrista.helpsocorrista.Services.Auth.AccessToken;
import br.com.helpsocorrista.helpsocorrista.util.SerializationUtil;

@DatabaseTable(tableName = Users.TABLE_NAME)
public class Users implements Parcelable, Serializable {
    public static final String TABLE_NAME                   = "users";
    public static final String COLUMN_ID                    = "id";
    public static final String COLUMN_CPF                   = "cpf";
    public static final String COLUMN_EMAIL                 = "email";
    public static final String COLUMN_SENHA                 = "password";
    public static final String COLUMN_SENHA_MOBILE          = "password_mobile";
    public static final String COLUMN_LAST_TOKEN            = "last_token";
    public static final String COLUMN_NOVA_SENHA            = "senha_reset";
    public static final String COLUMN_ALERGIAS              = "alergias_user";
    public static final String COLUNM_MEDICAM_CONTROLADOS   = "medicam_controlado_users";
    public static final String COLUNM_DOENCAS               = "doencas_users";

    @Expose
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, columnName = COLUMN_ID)
    private Integer id;


    @SerializedName("cpf")
    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_CPF)
    private String cpf;

    @SerializedName("username")
    @Expose
    @DatabaseField(columnName = COLUMN_EMAIL)
    private String email;

    @Expose(serialize = false)
    @DatabaseField(columnName = COLUMN_SENHA)
    private String senha;

    @SerializedName("password")
    @Expose
    @DatabaseField(canBeNull = false, columnName = COLUMN_SENHA_MOBILE)
    private String senhaMobile;

    @DatabaseField(columnName = COLUMN_LAST_TOKEN)
    private String last_token;

    @SerializedName("senha_reset")
    @DatabaseField(columnName = COLUMN_NOVA_SENHA)
    private String senha_reset;

    @SerializedName(COLUMN_ALERGIAS)
    @Expose
    @ForeignCollectionField(eager = true)
    private Collection<AlergiasUsers> alergiasUsersArrayList = new ArrayList<>();

    @SerializedName(COLUNM_MEDICAM_CONTROLADOS)
    @Expose
    @ForeignCollectionField(eager = true)
    private Collection<MedicamControladosUsers> medicamControladosUsersArrayList = new ArrayList<>();

    @SerializedName(COLUNM_DOENCAS)
    @Expose
    @ForeignCollectionField(eager = true)
    private Collection<DoencasUsers> doencasUsersArrayList = new ArrayList<>();

    @SerializedName("token")
    @Expose
    private AccessToken token;

    //construtor vazio
    public Users() {
    }


    // Getter and Setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenhaMobile() {
        return senhaMobile;
    }

    public void setSenhaMobile(String senhaMobile) {
        this.senhaMobile = senhaMobile;
    }

    public String getLast_token() {
        return last_token;
    }

    public void setLast_token(String last_token) {
        this.last_token = last_token;
    }

    public String getSenha_reset() {
        return senha_reset;
    }

    public void setSenha_reset(String senha_reset) {
        this.senha_reset = senha_reset;
    }

    public Collection<AlergiasUsers> getAlergiasUsersArrayList() {
        return alergiasUsersArrayList;
    }

    public void setAlergiasUsersArrayList(Collection<AlergiasUsers> alergiasUsersArrayList) {
        this.alergiasUsersArrayList = alergiasUsersArrayList;
    }

    public Collection<MedicamControladosUsers> getMedicamControladosUsersArrayList() {
        return medicamControladosUsersArrayList;
    }

    public void setMedicamControladosUsersArrayList(Collection<MedicamControladosUsers> medicamControladosUsersArrayList) {
        this.medicamControladosUsersArrayList = medicamControladosUsersArrayList;
    }

    public Collection<DoencasUsers> getDoencasUsersArrayList() {
        return doencasUsersArrayList;
    }

    public void setDoencasUsersArrayList(Collection<DoencasUsers> doencasUsersArrayList) {
        this.doencasUsersArrayList = doencasUsersArrayList;
    }

    public AccessToken getAccessToken() {
        return token;
    }

    public void setAccessTokenn(AccessToken token) {
        this.token = token;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.id);
        dest.writeString(this.cpf);
        dest.writeString(this.email);
        dest.writeString(this.senha);
        dest.writeString(this.senhaMobile);
        dest.writeString(this.senha_reset);
        try {
            dest.writeString(SerializationUtil.toString((Serializable) this.alergiasUsersArrayList));
            dest.writeString(SerializationUtil.toString((Serializable) this.doencasUsersArrayList));
            dest.writeString(SerializationUtil.toString((Serializable) this.medicamControladosUsersArrayList));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Users(Parcel in){
        this.id             = (Integer) in.readSerializable();
        this.cpf            = in.readString();
        this.email          = in.readString();
        this.senha          = in.readString();
        this.senhaMobile    = in.readString();
        this.senha_reset    = in.readString();
        try {
            this.alergiasUsersArrayList             = (Collection<AlergiasUsers>) SerializationUtil.fromString(in.readString());
            this.doencasUsersArrayList              = (Collection<DoencasUsers>) SerializationUtil.fromString(in.readString());
            this.medicamControladosUsersArrayList   = (Collection<MedicamControladosUsers>) SerializationUtil.fromString(in.readString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static final Creator<Users> CREATOR = new Creator<Users>() {
        @Override
        public Users createFromParcel(Parcel source) {
            return new Users(source);
        }

        @Override
        public Users[] newArray(int size) {
            return new Users[size];
        }
    };


}
