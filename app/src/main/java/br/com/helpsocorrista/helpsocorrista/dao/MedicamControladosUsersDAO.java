package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.model.MedicamControladosUsers;

public class MedicamControladosUsersDAO {

    private Dao<MedicamControladosUsers, Integer> dao;

    private Dao<MedicamControladosUsers, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(MedicamControladosUsers.class);
        }
        return dao;
    }

    public void save(MedicamControladosUsers medicamControladosUsers) throws SQLException{
        if(medicamControladosUsers.getId() == null || medicamControladosUsers.getId() == 0){
            getDao().create(medicamControladosUsers);
        } else {
            if (getById(medicamControladosUsers.getId()) == null) {
                getDao().create(medicamControladosUsers);
            } else {
                getDao().update(medicamControladosUsers);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public MedicamControladosUsers getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }
}
