package br.com.helpsocorrista.helpsocorrista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.helpsocorrista.helpsocorrista.Helpers.enums.GrauUrgencia;
import br.com.helpsocorrista.helpsocorrista.Helpers.enums.Sexo;
import br.com.helpsocorrista.helpsocorrista.dao.SolicitacaoDAO;
import br.com.helpsocorrista.helpsocorrista.model.Solicitacao;
import br.com.helpsocorrista.helpsocorrista.util.CustomSection;
import br.com.helpsocorrista.helpsocorrista.util.ListaSolicitacaoActivity;

public class RealizarSolicitacaoActivity extends AppCompatActivity {


    private EditText    txtDescriçãoRS;
    private Spinner     myGrauUrgencia;
    private Button      btnCancelarRS;
    private Button      btnSalvarRS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realizar_solicitacao);

        txtDescriçãoRS  = (EditText)findViewById(R.id.txtDescriçãoRS);
        btnCancelarRS   = (Button)findViewById(R.id.btnCancelarRS);
        btnSalvarRS     = (Button)findViewById(R.id.btnSalvarRS);
        myGrauUrgencia  = (Spinner)findViewById(R.id.txtGrauUrgencia);
        myGrauUrgencia.setAdapter(new ArrayAdapter<GrauUrgencia>(this, android.R.layout.simple_list_item_1, GrauUrgencia.values()));


        btnSalvarRS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String descricao = txtDescriçãoRS.getText().toString();

                if (myGrauUrgencia.getSelectedItemId() == 0){
                    Toast.makeText(getApplicationContext(), "Obrigatório informar um Grau de Urgencia", Toast.LENGTH_SHORT).show();
                    return;
                }

                Solicitacao solicitacao = new Solicitacao();
                solicitacao.setUsers_id(CustomSection.getInstance().getLoggedUser());

                solicitacao.setDescricao_solicitacao(descricao);
                solicitacao.setGrau_de_urgencia(String.valueOf((GrauUrgencia)myGrauUrgencia.getSelectedItem()));

                try {
                    new SolicitacaoDAO().save(solicitacao);
                    Toast.makeText(getApplicationContext(), "Solicitação realizada com suceoss...", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getApplicationContext(), SolicitacaoActivity.class);
                    startActivity(i);
                    finish();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });

        btnCancelarRS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RealizarSolicitacaoActivity.this, SolicitacaoActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

}
