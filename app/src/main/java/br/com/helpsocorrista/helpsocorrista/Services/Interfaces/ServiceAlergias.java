package br.com.helpsocorrista.helpsocorrista.Services.Interfaces;

import br.com.helpsocorrista.helpsocorrista.Services.dto.AlergiaDto;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ServiceAlergias {

//    @Headers({
//            "Authorization: Token e4bf771bbc5916073f8f3d1777a861e89eb71a67"
//    })

    @GET("alergia")
    Call<AlergiaDto> listar();
}
