package br.com.helpsocorrista.helpsocorrista.util;

import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationUtil {

    public static Object fromString(String s) throws IOException, ClassNotFoundException {
        if (s != null) {
            final byte[] data = Base64.decode(s, Base64.DEFAULT);
            final ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
            final Object obj = ois.readObject();
            ois.close();

            return obj;
        }
        return null;
    }

    public static String toString(Serializable object) throws IOException {
        if (object != null) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final ObjectOutputStream oos = new ObjectOutputStream(baos);

            oos.writeObject(object);
            oos.close();

            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        }
        return null;
    }
}
