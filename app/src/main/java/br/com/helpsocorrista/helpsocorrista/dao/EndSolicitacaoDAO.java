package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import br.com.helpsocorrista.helpsocorrista.model.EndSolicitacao;

public class EndSolicitacaoDAO {

    private Dao<EndSolicitacao, Integer> dao;

    private Dao<EndSolicitacao, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(EndSolicitacao.class);
        }
        return dao;
    }

    public void save(EndSolicitacao endSolicitacao) throws SQLException{
        if(endSolicitacao.getId() == null || endSolicitacao.getId() == 0){
            getDao().create(endSolicitacao);
        } else {
            if (getById(endSolicitacao.getId()) == null) {
                getDao().create(endSolicitacao);
            } else {
                getDao().update(endSolicitacao);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }


    public EndSolicitacao getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

}
