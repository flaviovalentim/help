package br.com.helpsocorrista.helpsocorrista.dao;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import br.com.helpsocorrista.helpsocorrista.model.Solicitacao;


public class SolicitacaoDAO {
    private Dao<Solicitacao, Integer> dao;

    private Dao<Solicitacao, Integer> getDao() throws SQLException {
        if (dao == null) {
            dao = DBHelper.getmInstance().getDao(Solicitacao.class);
        }
        return dao;
    }

    public void save(Solicitacao solicitacao) throws SQLException{
        if(solicitacao.getId() == null || solicitacao.getId() == 0){
            getDao().create(solicitacao);
        } else {
            if (getById(solicitacao.getId()) == null) {
                getDao().create(solicitacao);
            } else {
                getDao().update(solicitacao);
            }
        }

        DBHelper.getmInstance().relaseHelper();
    }

    public Solicitacao getById(Integer id) throws SQLException {
        return getDao().queryForId(id);
    }

}
