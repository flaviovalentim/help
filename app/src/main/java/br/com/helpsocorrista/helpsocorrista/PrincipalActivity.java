package br.com.helpsocorrista.helpsocorrista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PrincipalActivity extends AppCompatActivity {

    private Button btnAcesso;
    private Button btnPrimeiroAcesso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btnAcesso           = (Button) findViewById(R.id.btnAcesso);
        btnPrimeiroAcesso   = (Button) findViewById(R.id.btnPrimeiroAcesso);
        btnAcesso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PrincipalActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });



        btnPrimeiroAcesso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PrincipalActivity.this, CadastroUsersActivity.class);
                startActivity(i);
            }
        });


    }

}
