package br.com.helpsocorrista.helpsocorrista.model;

public interface TabelaAuxiliar {
    String getTextoCurto();

    Integer getId();

    void setId(Integer id);
}
