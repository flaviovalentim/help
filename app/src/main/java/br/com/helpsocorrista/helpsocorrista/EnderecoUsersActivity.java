package br.com.helpsocorrista.helpsocorrista;

import android.content.Intent;
import android.location.Address;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.util.concurrent.ExecutionException;

import br.com.helpsocorrista.helpsocorrista.Services.HttpService;
import br.com.helpsocorrista.helpsocorrista.model.Cep;
import br.com.helpsocorrista.helpsocorrista.model.DadosPessoais;

public
class EnderecoUsersActivity extends AppCompatActivity {

    private EditText        txtCep;
    private EditText        txtLogradouro;
    private EditText        txtNumero;
    private EditText        txtBairro;
    private EditText        txtPontoReferencia;
    private EditText        txtComplemento;
    private Address         endereco;
    private Button          btnLocalizacaoEnd;
    private Button          btnCacelarEnd;
    private Button          btnAvancarEnd;
    private DadosPessoais   dadosPessoais;

    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        dadosPessoais = (DadosPessoais) i.getSerializableExtra("dados");
        setContentView(R.layout.activity_endereco_users);


        txtCep                  = (EditText) findViewById(R.id.txtCep);
        txtLogradouro           = (EditText)findViewById(R.id.txtLogradouro);
        txtNumero               = (EditText)findViewById(R.id.txtNumero);
        txtBairro               = (EditText)findViewById(R.id.txtBairro);
        txtPontoReferencia      = (EditText)findViewById(R.id.txtPontoReferencia);
        txtComplemento          = (EditText)findViewById(R.id.txtComplemento);
        btnLocalizacaoEnd       = (Button)findViewById(R.id.btnLocalizacaoEnd);
        btnCacelarEnd           = (Button)findViewById(R.id.btnCacelarEnd);
        btnAvancarEnd           = (Button) findViewById(R.id.btnAvancarEnd);


        btnAvancarEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String logradouro       = StringUtil.toMaiuscula(txtLogradouro.getText().toString());
//                String bairro           = StringUtil.toMaiuscula(txtBairro.getText().toString());
//                String pontoReferencia  = StringUtil.toMaiuscula(txtPontoReferencia.getText().toString());
//                String complemento      = StringUtil.toMaiuscula(txtComplemento.getText().toString());
//
                String logradouro       = txtLogradouro.getText().toString();
                String bairro           = txtBairro.getText().toString();
                String pontoReferencia  = txtPontoReferencia.getText().toString();
                String complemento      = txtComplemento.getText().toString();
                String cep              = txtCep.getText().toString().replaceAll("[^0-9]", "");
                String numero           = txtNumero.getText().toString().replaceAll("[^0-9]", "");

                if (cep.equals("")){
                    Toast.makeText(getApplicationContext(), "Obrigatório informar Cep.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (logradouro.equals("")){
                    Toast.makeText(getApplicationContext(), "Obrigatório informar Logradouro.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (numero.equals("")){
                    Toast.makeText(getApplicationContext(), "Obrigatório informar Número.", Toast.LENGTH_SHORT).show();
                    return;
                }

                dadosPessoais.setCep(Integer.parseInt(cep));
                dadosPessoais.setLogradouro(logradouro);
                dadosPessoais.setNumero(Integer.parseInt(numero));
                dadosPessoais.setBairro(bairro);
                dadosPessoais.setPonto_referencia(pontoReferencia);
                dadosPessoais.setComplemento(complemento);

                Intent i = new Intent(EnderecoUsersActivity.this, DadosClinicosActivity.class);
                i.putExtra("dados", dadosPessoais);
                startActivity(i);
                finish();
            }
        });
        btnLocalizacaoEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { obterEndereço();
            }
        });

        btnCacelarEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EnderecoUsersActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        //INICIO DA MASCARA
        SimpleMaskFormatter sfcep   = new SimpleMaskFormatter("NNNNN - NNN");
        MaskTextWatcher mtwcep      = new MaskTextWatcher(txtCep, sfcep);
        txtCep.addTextChangedListener(mtwcep);
        //FIM DA MASCARA

    }
    private void obterEndereço(){
        String cep = txtCep.getText().toString().replaceAll("[^0-9]", "");
        if (cep.equals("")){
            Toast.makeText(getApplicationContext(), "Obrigatório informar Cep.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (cep.length() < 8){
            Toast.makeText(getApplicationContext(), "Obrigatório informar Cep válido.", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            Cep retorno = new HttpService(cep).execute().get();
            if (retorno == null){
                Toast.makeText(getApplicationContext(), "Cep informado é inválido.", Toast.LENGTH_SHORT).show();
                return;
            }
            txtLogradouro.setText(retorno.getLogradouro());
            txtBairro.setText(retorno.getBairro());
            txtComplemento.setText(retorno.getComplemento());
            Log.i("END", String.valueOf(retorno));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

}

