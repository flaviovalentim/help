package br.com.helpsocorrista.helpsocorrista.Helpers.enums;

public enum GrauUrgencia {
    S("Selecione"),
    M("Baixo"),
    F("Médio"),
    OT("Grave"),
    NI("Gravíssimo");

    private String descricao;

    private GrauUrgencia(String descricao){
    this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
